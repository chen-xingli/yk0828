package cn.geek51.controller;

import cn.geek51.domain.*;
import cn.geek51.service.IDeviceinfoService;

import cn.geek51.util.ResponseUtil;
import cn.geek51.util.UserContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 设备控制器
 */
@RestController
public class DeviceinfoController {
    @Autowired
    IDeviceinfoService iDeviceinfoService;



    // 新建设备
    @PostMapping("/imformation")
    public Object insertEmployee(Deviceinfo deviceinfo) {
        UserAuth user = UserContext.getCurrentUser();
        deviceinfo.setArea(user.getArea());
        iDeviceinfoService.save(deviceinfo);
        return ResponseUtil.general_response("success insert deviceinfo!");
    }

    // 查询, query传入LAYUI查询参数
    @GetMapping("/imformation")
    public Object getEmployees(PageHelper pageHelper, String query) throws Exception {
        Map<Object, Object> map = pageHelper.getMap();
        Map<Object, Object> maps = pageHelper.getMap();

        HashMap queryMap = null;
        //进行拼接, 拼接成一个MAP查询
        if (query != null) {
            queryMap = new ObjectMapper().readValue(query, HashMap.class);
            map.putAll(queryMap);
        }
        UserAuth user = UserContext.getCurrentUser();
        map.put("Area",user.getArea());
        maps.put("Area",user.getArea());
        List<Deviceinfo> deviceinfoList = iDeviceinfoService.listAll(map);
        if (map == null) map = new HashMap<>();
        else map.clear();
        map.put("size", iDeviceinfoService.counts(maps));
        return ResponseUtil.general_response(deviceinfoList, map);
    }

     //每30分钟查一次设备状态
    @Scheduled(cron = "0 */30 * * * ?")
    public Object getEmployeet() throws Exception {
        Map<Object, Object> map = new HashMap<Object, Object>();
        HashMap queryMap = null;
        //进行拼接, 拼接成一个MAP查询
        List<Deviceinfo> deviceinfoList = iDeviceinfoService.listAll(map);
        //用来ping IP地址是否可以连接
        for (Deviceinfo deviceinfo : deviceinfoList) {
            deviceinfo.setID(deviceinfo.getID());
            boolean status = InetAddress.getByName(deviceinfo.getIP()).isReachable(3000);
            if (status) {
                deviceinfo.setOnline("在线");
            } else {
                deviceinfo.setOnline("离线");
            }
            iDeviceinfoService.updateFile(deviceinfo);
        }
        if (map == null) map = new HashMap<>();
        else map.clear();
        map.put("size", iDeviceinfoService.count());
        return ResponseUtil.general_response(deviceinfoList, map);
    }



    //修改设备信息
    @PutMapping("/imformation")
    public Object updateEmployee(@RequestBody Map<String, String> map) {
        Deviceinfo deviceinfo = new Deviceinfo();
        System.out.println("ststionid" + map.get("ststionid"));
        deviceinfo.setSTSTIONID(map.get("ststionid"));
        deviceinfo.setID(Integer.parseInt(map.get("id")));
        deviceinfo.setDEVICEBRAND(map.get("devicebrand"));
        deviceinfo.setDEVICEMODEL(map.get("devicemodel"));
        deviceinfo.setDEVICENAME(map.get("devicename"));
        deviceinfo.setDEVICEVERSION(map.get("deviceversion"));
        deviceinfo.setIP(map.get("ip"));
        deviceinfo.setREMARK(map.get("remark"));
        iDeviceinfoService.update(deviceinfo);
        return ResponseUtil.general_response("success update employee!");
    }

    // 删除
    @DeleteMapping("/imformation/{id}")
    public Object deleteEmployee(@PathVariable("id") Integer id) {
        iDeviceinfoService.delete(id);
        return ResponseUtil.general_response("success delete employee!");
    }





}
