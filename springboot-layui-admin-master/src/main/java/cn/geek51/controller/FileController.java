package cn.geek51.controller;

import cn.geek51.domain.Deviceinfo;

import cn.afterturn.easypoi.word.entity.WordImageEntity;

import cn.geek51.domain.MaintenanceInfo;
import cn.geek51.domain.StationInfo;
import cn.geek51.service.*;
import cn.geek51.util.ResponseUtil;
import cn.hutool.core.io.FileUtil;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.PictureRenderData;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@Slf4j
public class FileController {
    @Autowired
    InspectService inspectService;
    @Autowired
    IfileService ifileService;
    @Autowired
    IStationInfoService iStationInfoService;
    @Autowired
    IMaintenanceService iMaintenanceService;
    @Autowired
    IDeviceinfoService iDeviceinfoService;


    @GetMapping("/files/download/{id}")
    public String downloadFile(@PathVariable("id") int id,HttpServletResponse response) throws Exception {
        String downloadPath=ifileService.listOneById(id).getFILEPATH();
        String filename=ifileService.listOneById(id).getFILENAME();
        System.out.println(downloadPath+"文件地址");
        File file = new File(downloadPath);
        if (file.exists()) {
            response.setContentType("application/force-download");// 设置强制下载不打开
            response.addHeader("Content-Disposition", "attachment;fileName=" + filename);// 设置文件名
            System.out.println("下载的文件名称:"+filename);
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
                System.out.println("success");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return null;
    }
    @GetMapping("/files/outPut/{id}")
    public String outPut(@PathVariable("id") int id,HttpServletRequest request,HttpServletResponse response) throws FileNotFoundException {
        HashMap<String, Object> dataMap = new HashMap<>();
        MaintenanceInfo maintenanceInfo = iMaintenanceService.listOneById(id);
//        List<cn.geek51.domain.File> filess=ifileService.listmain(id);
//
//        System.out.println(filess.get(0).getFILEPATH()+"查看数据");
//
//        for (int i = 0; i < filess.size(); i++) {
//            File dataFile = new File(filess.get(i).getFILEPATH());
//
//            System.out.println(filess.get(i).getFILEPATH()+"pic");
//            dataMap.put("image"+i,new  PictureRenderData(200, 185,filess.get(i).getFILEPATH()));
//        }
        Date date = new Date();
        String dateStr="";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        dateStr = simpleDateFormat.format(date);
        dataMap.put("code","YK"+dateStr);
        dataMap.put("subtime",maintenanceInfo.getSUBMITTIME());
        dataMap.put("handtime",maintenanceInfo.getHANDLETIME());
        dataMap.put("username",maintenanceInfo.getSUBMITTER()+  ","  +maintenanceInfo.getHANDLER());
        dataMap.put("statname","公路段");
//        dataMap.put("telphone","");
        dataMap.put("desc",maintenanceInfo.getDESCRIPTION());
        dataMap.put("handldesc",maintenanceInfo.getHANDLEREMARK());
        dataMap.put("remark",maintenanceInfo.getREMARK());
        try {
//            ClassPathResource template = new ClassPathResource("word/demoTemplate.docx");

            String filePath = "D:\\demoTemplate.docx";
            //关键一部操作   里面详细过程有兴趣可以翻阅下源码
            XWPFTemplate xwpfTemplate = XWPFTemplate.compile(filePath).render(dataMap);
            String docName = System.currentTimeMillis()+ ".docx";
            File targetFile = new File(docName);
            FileOutputStream out = new FileOutputStream(targetFile);
            xwpfTemplate.write(out);
            out.flush();
            out.close();
            xwpfTemplate.close();
            // 下载输出到浏览器
            downFile(request,response,docName,targetFile);
            FileUtil.del(targetFile);
//          FileUtil.deleteDir(targetFile.getPath());
        } catch (Exception e) {
//          log.info("文件生成失败："+e.getMessage());
            throw new RuntimeException("文件生成失败："+e.getMessage());
        }
        return null;
    }




    @GetMapping("/files/stoutPut/{id}")
    public String stoutPut(@PathVariable("id") int id,HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException{
        HashMap<String, Object> dataMap = new HashMap<>();
        BASE64Encoder encoder = new BASE64Encoder();
        StationInfo stationInfo = iStationInfoService.listOneById(id);
        Date date = new Date();
        String dateStr="";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        dateStr = simpleDateFormat.format(date);
        dataMap.put("time",stationInfo.getCreateTime());
        dataMap.put("stname",stationInfo.getStationName());
        dataMap.put("areaname",stationInfo.getAreaName());
        dataMap.put("proname",stationInfo.getPROJECTNAME());
        dataMap.put("roadcode",stationInfo.getRoadCode());
        dataMap.put("insttype",stationInfo.getINSTALLTYPE());
        dataMap.put("instplace",stationInfo.getINSTALLPLACE());
        dataMap.put("lng",stationInfo.getLng());
        dataMap.put("lat",stationInfo.getLat());
        dataMap.put("pixel",stationInfo.getPIXEL());
        dataMap.put("devicetype",stationInfo.getDEVICETYPE());
        dataMap.put("powertype",stationInfo.getPOWERTYPE());
        dataMap.put("telphone",stationInfo.getTELPHONE());
        dataMap.put("eleplace","接电");
        dataMap.put("worktype",stationInfo.getWORKTYPE());
        dataMap.put("bulength",stationInfo.getBUILTLENGTH());
        dataMap.put("powerlength",stationInfo.getPOWERLENGTH());
        dataMap.put("network",stationInfo.getNETWORK());
        File dataFile = new File(stationInfo.getFILE());
        byte[] bytes = fileConvertToByteArray(dataFile);
        dataMap.put("image",encoder.encode(bytes));
        dataMap.put("image1",encoder.encode(bytes));
        Configuration configuration = new Configuration();
        configuration.setDefaultEncoding("utf-8");
        try {
            configuration.setDirectoryForTemplateLoading(new File("D:/newjar"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String uploadDocsPath = "D:\\ywjlStout\\"+dateStr;
        File dir=new File(uploadDocsPath);
        if (!dir.exists()){
            dir.mkdir();
        }
        String substring = UUID.randomUUID().toString().substring(0, 7);
        File outFile = new File("D:/ywjl/"+dateStr+"/"+substring+".docx");//输出路径
        Template t=null;
        Writer out = null;
        try {
            t = configuration.getTemplate("station.ftl", "utf-8"); //文件名，获取模板
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"));
            t.process(dataMap, out);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
        //D:\yunwei\chulifile\2020-11-06\timg122.jpg下载

        String path="D:\\ywjl\\"+dateStr+"\\"+substring+".docx";
        System.out.println("下载路径 = " + path);
        String fileName=stationInfo.getStationName()+".docx";
        System.out.println("文件名 = " + fileName);
        File file = new File(path);
        try {
            downFile(request,response,fileName,file);
            FileUtil.del(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    /**
     * 新增接口
     * @param id
     * @param request
     * @param response
     * @return
     * @throws FileNotFoundException
     */
    @GetMapping("/files/stoutPutByPoi/{id}")
    public String stoutPutByPoi(@PathVariable("id") int id, HttpServletRequest request,HttpServletResponse response) throws FileNotFoundException{
        Map<String, Object> dataMap = new HashMap<>();
        StationInfo stationInfo = iStationInfoService.listOneById(id);
        String id1=Integer.toString(id);
        String a =null;
        Deviceinfo d=iDeviceinfoService.listByStId(id1);
        System.out.println("d = " + d);
//        System.out.println("aaaaa0"+iDeviceinfoService.listByStId(id1).getDEVICENAME());
        if (iDeviceinfoService.listByStId(id1)==null||iDeviceinfoService.listByStId(id1).equals("")){
            dataMap.put("device","暂未添加");
        }else
        {
            dataMap.put("device",iDeviceinfoService.listByStId(id1).getDEVICENAME());
        }
        dataMap.put("time",stationInfo.getCreateTime());
        dataMap.put("stname",stationInfo.getStationName());
        dataMap.put("areaname",stationInfo.getAreaName());
        dataMap.put("proname",stationInfo.getPROJECTNAME());
        dataMap.put("roadcode",stationInfo.getRoadCode());
        dataMap.put("insttype",stationInfo.getINSTALLTYPE());
        dataMap.put("instplace",stationInfo.getINSTALLPLACE());
        dataMap.put("lng",stationInfo.getLng());
        dataMap.put("lat",stationInfo.getLat());
        dataMap.put("pixel",stationInfo.getPIXEL());
        if (iDeviceinfoService.listByStId(id1)==null||iDeviceinfoService.listByStId(id1).equals("")){
            dataMap.put("device","暂未添加");
        }else
        {
            dataMap.put("devicetype",iDeviceinfoService.listByStId(id1).getDEVICEMODEL());
        }
        dataMap.put("devicetype",stationInfo.getDEVICETYPE());
        dataMap.put("powertype",stationInfo.getPOWERTYPE());
        dataMap.put("telphone",stationInfo.getTELPHONE());
        dataMap.put("eleplace","接电");
        dataMap.put("worktype",stationInfo.getWORKTYPE());
        dataMap.put("bulength",stationInfo.getBUILTLENGTH());
        dataMap.put("powerlength",stationInfo.getPOWERLENGTH());
        dataMap.put("network",stationInfo.getNETWORK());
        //判断有两条或者以上的记录时
        if (stationInfo.getFILE()!=null&&stationInfo.getFILE().contains(",")){
            //可以设置照片路径，截取。
            int index = stationInfo.getFILE().indexOf(",");
            //第一张
            String pic1 = stationInfo.getFILE().substring(0, index);
            System.out.println(pic1);
            //第二张
            String pic2= stationInfo.getFILE().substring(index+1);
            System.out.println(pic2);
            dataMap.put("image1",new  PictureRenderData(200, 185,pic1));
            dataMap.put("image2",new  PictureRenderData(200, 185,pic2));
        }else if (stationInfo.getFILE()!=null){  //如果是单条记录
            String pic1=stationInfo.getFILE();
            dataMap.put("image1",new PictureRenderData(200,185,pic1));
        }else if (stationInfo.getFILE()==null|stationInfo.getFILE()==""|stationInfo.getFILE().equals("")){
            dataMap.put("image1",null);
            dataMap.put("image2",null);
        }
        try {
//            ClassPathResource template = new ClassPathResource("word/demo.docx");
            String filePath = "D:\\demo.docx";
            XWPFTemplate xwpfTemplate = XWPFTemplate.compile(filePath).render(dataMap);
            String docName = System.currentTimeMillis()+ ".docx";
            File targetFile = new File(docName);
            FileOutputStream out = new FileOutputStream(targetFile);
            xwpfTemplate.write(out);
            out.flush();
            out.close();
            xwpfTemplate.close();
            // 下载输出到浏览器
            downFile(request,response,docName,targetFile);
            FileUtil.del(targetFile);
        } catch (Exception e) {
//            log.info("文件生成失败："+e.getMessage());
            throw new RuntimeException("文件生成失败："+e.getMessage());
        }

        return null;

    }

    @RequestMapping("/files/getList")
    public Map<Object, Object> getList(cn.geek51.domain.File file){
        List<cn.geek51.domain.File> fileList = ifileService.listAll(file);
        return ResponseUtil.general_response(fileList);
    }

    /**
     * 把一个文件转化为byte字节数组。
     *
     * @return byte[]
     */
    private byte[] fileConvertToByteArray(File file) {
        byte[] data = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            int len;
            byte[] buffer = new byte[1024];
            while ((len = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            data = baos.toByteArray();
            fis.close();
            baos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * 下载文件到浏览器
     * @param request
     * @param response
     * @param filename 要下载的文件名
     * @param file     需要下载的文件对象
     * @throws IOException
     */
    public static void downFile(HttpServletRequest request, HttpServletResponse response, String filename, File file) throws IOException {
        //  文件存在才下载
        if (file.exists()) {
            OutputStream out = null;
            FileInputStream in = null;
            try {
                // 1.读取要下载的内容
                in = new FileInputStream(file);

                // 2. 告诉浏览器下载的方式以及一些设置
                // 解决文件名乱码问题，获取浏览器类型，转换对应文件名编码格式，IE要求文件名必须是utf-8, firefo要求是iso-8859-1编码
                String agent = request.getHeader("user-agent");
                if (agent.contains("FireFox")) {
                    filename = new String(filename.getBytes("UTF-8"), "iso-8859-1");
                } else {
                    filename = URLEncoder.encode(filename, "UTF-8");
                }
                // 设置下载文件的mineType，告诉浏览器下载文件类型
                String mineType = request.getServletContext().getMimeType(filename);
                response.setContentType(mineType);
                // 设置一个响应头，无论是否被浏览器解析，都下载
                response.setHeader("Content-disposition", "attachment; filename=" + filename);
                // 将要下载的文件内容通过输出流写到浏览器
                out = response.getOutputStream();
                int len = 0;
                byte[] buffer = new byte[1024];
                while ((len = in.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            }
        }
    }
}