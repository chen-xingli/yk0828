package cn.geek51.controller;

import cn.geek51.domain.*;

import cn.geek51.service.IStationInfoService;
import cn.geek51.service.IfileService;
import cn.geek51.service.InspectService;
import cn.geek51.util.ResponseUtil;
import cn.geek51.util.UserContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.*;

/**
 * 设备控制器
 */
@RestController
public class InspectController {
    @Autowired
    InspectService inspectService;
    @Autowired
    IStationInfoService stationInfoService;
    @Autowired
    IfileService ifileService;
    // 查询, query传入LAYUI查询参数
    @GetMapping("/inspect")
    public Object getInspect(PageHelper pageHelper, String query) throws Exception {
        Map<Object, Object> map = pageHelper.getMap();
        Map<Object, Object> maps = pageHelper.getMap();
        HashMap queryMap = null;
        //进行拼接, 拼接成一个MAP查询
        if (query != null) {
            queryMap = new ObjectMapper().readValue(query, HashMap.class);
            map.putAll(queryMap);
        }
        //获取当前登录人所属区域
        UserAuth user = UserContext.getCurrentUser();
        map.put("Area",user.getArea());
        //条数查询
        maps.put("Area",user.getArea());
        List<Inspect> inspects = inspectService.listAll(map);
        if (map == null) map = new HashMap<>();
        else map.clear();
        map.put("size", inspectService.counts(maps));
        return ResponseUtil.general_response(inspects, map);
    }


    //巡检信息
    @PutMapping("/inspect")
    public Object updateInspect(@RequestBody Map<String, String> map, Model model) throws ParseException {
          String id=map.get("id");
          String filename=map.get("inspectphoto");
          String string = filename;
          File file=new File();
        //长度5 下标从0开始 到4
        String substring = string.substring(0, string.length() - 1);
        //以逗号分割，得出的数据存到 result 里面
        String[] result = substring.split(",");
        //设置文件路径
        for (String r : result) {
            System.out.println(r);
            String Filepath="D:\\images\\"+r;
            file.setFILENAME(r);
            file.setFILEPATH(Filepath);
            file.setINSPECTID(id);
            ifileService.save(file);
        }
        UserAuth user = UserContext.getCurrentUser();
        Inspect inspect = new Inspect();
        //编辑巡检信息
        inspect.setINSPECTER(user.getUsername());
        inspect.setID(Integer.parseInt(map.get("id")));
        inspect.setINSPECTREMARK(map.get("inspectremark"));
        inspect.setINSPECTPHOTO(map.get("inspectphoto"));
        inspect.setSTATUS("0");
        inspectService.update(inspect);
        //查本次日期和巡检周期
        StationInfo stationInfo=stationInfoService.listNameById(Integer.parseInt(map.get("stationid")));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(map.get("INSPECTDATE"));
        System.out.println("date:"+date);
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        //当前运维日期加巡检周期
        cal.add(cal.MONTH, Integer.parseInt(stationInfo.getCycle()));
        Date zhouqi = cal.getTime();
        Date date2 = zhouqi;         //获得你要处理的时间 Date型
        String strDate= sdf.format(date2); //格式化成yyyy-MM-dd格式的时间字符串
        Date newDate =sdf.parse(strDate);
        java.sql.Date resultDate = new java.sql.Date(newDate.getTime());
        Inspect upinspect = new Inspect();
        upinspect.setSTATIONID(map.get("stationid"));
        upinspect.setINSPECTDATE(resultDate);
        //获取当前用户所在区域
        upinspect.setAREA(user.getArea());
        inspectService.upsave(upinspect);
        return ResponseUtil.general_response("success update inspect!");
    }

    @GetMapping("/getFile/{INSPECTID}")
    public Object getStationById(@PathVariable("INSPECTID") Integer INSPECTID){
       List<File> file = ifileService.listLLAll(INSPECTID);
        return  ResponseUtil.general_response(file);
    }


}




