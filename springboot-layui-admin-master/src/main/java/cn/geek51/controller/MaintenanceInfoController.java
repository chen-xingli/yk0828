package cn.geek51.controller;

import cn.geek51.domain.*;
import cn.geek51.service.IMaintenanceService;
import cn.geek51.service.IfileService;
import cn.geek51.util.ResponseUtil;
import cn.geek51.util.UserContext;
import com.alibaba.fastjson.JSON;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class MaintenanceInfoController {
    @Autowired
    IMaintenanceService iMaintenanceService;
    @Autowired
    IfileService ifileService;


    @PostMapping("/insertMaintenance")
    public Object insertMaintenanceInfo(MaintenanceInfo maintenanceInfo,@RequestParam(value = "fileIds[]",required = false) List<Integer> fileIds) {
        UserAuth user = UserContext.getCurrentUser();
        maintenanceInfo.setAREA(user.getArea());
        if (maintenanceInfo.getHANDLEREMARK()==null||maintenanceInfo.getHANDLEREMARK().equals("null")){
        maintenanceInfo.setHANDLEREMARK("未处理");
   }
        if(CollectionUtils.isEmpty(fileIds)){
            iMaintenanceService.save(maintenanceInfo);
        }else{
            iMaintenanceService.save(maintenanceInfo,fileIds);  //保存
        }
        return ResponseUtil.general_response("success insert stationInfo!");
    }
    @PostMapping("/insertMaintenance1")
    public Object insertMaintenanceInfo1(MaintenanceInfo maintenanceInfo) {
        UserAuth user = UserContext.getCurrentUser();
        maintenanceInfo.setAREA(user.getArea());
        if (maintenanceInfo.getHANDLEREMARK()==null||maintenanceInfo.getHANDLEREMARK().equals("null")){
            maintenanceInfo.setHANDLEREMARK("未处理");
        }
        iMaintenanceService.save(maintenanceInfo);
        return ResponseUtil.general_response("success insert stationInfo!");
    }



    // 查询, query传入LAYUI查询参数
    @GetMapping("/selectMaintenance")
    public Object getStationInfo(PageHelper pageHelper, String query) throws Exception {
        Map<Object, Object> map = pageHelper.getMap();
        Map<Object, Object> maps = pageHelper.getMap();
        HashMap queryMap = null;
        //进行拼接, 拼接成一个MAP查询
        if (query != null) {
            queryMap = new ObjectMapper().readValue(query, HashMap.class);
            map.putAll(queryMap);
        }
        UserAuth user = UserContext.getCurrentUser();
        map.put("Area",user.getArea());
        //条数查询
        maps.put("Area",user.getArea());
        List<MaintenanceInfo> deviceinfoList = iMaintenanceService.listAll(map);
        System.out.println("deviceinfoList = " + deviceinfoList);
        // map重用, 用于回传pagesize asd as das as d sa as  h
        if (map == null) map = new HashMap<>();
        else map.clear();
        map.put("size", iMaintenanceService.counts(maps));
        return ResponseUtil.general_response(deviceinfoList, map);
    }



     //更改
    @PutMapping("/updateMaintenance")
    public Object updateStationInfo(@RequestBody Map<String, String> map) {
        MaintenanceInfo maintenanceInfo = new MaintenanceInfo();
        //System.out.println("ststionid"+map.get("ststionid"));
        maintenanceInfo.setID(Integer.parseInt(map.get("ID")));
//        stationInfo.setStationName(map.get("stationname"));
//        stationInfo.setDirection(map.get("direction"));
//        stationInfo.setAreaName(map.get("areaname"));
//        stationInfo.setRoadCode(map.get("roadcode"));
//        stationInfo.setRoadName(map.get("roadname"));
//        stationInfo.setStakeNumber(map.get("stakenumber"));
//        stationInfo.setLng(map.get("lng"));
//        stationInfo.setLat(map.get("lat"));
//        stationInfo.setDeviceCode(map.get("devicecode"));
//        stationInfo.setLaneNumber(Integer.parseInt(map.get("lanenumber")));
//        stationInfo.setCreateTime(Timestamp.valueOf(map.get("createtime")));
//        stationInfo.setCycle(map.get("cycle"));
        maintenanceInfo.setREMARK(map.get("REMARK"));
        maintenanceInfo.setHANDLER(map.get("HANDLER"));
        maintenanceInfo.setHANDLEREMARK("已处理");
        iMaintenanceService.update(maintenanceInfo);
        return ResponseUtil.general_response("success update employee!");
}
    @PutMapping("/updateMaintenanceById")
    @ResponseBody
public Map<Object,Object> updateMaintenanceById(@RequestBody Map<Object, Object> map){
        List<Integer> fileIds = JSON.parseArray(map.get("fileIds").toString(), Integer.class);
        MaintenanceInfo maintenanceInfo1= JSON.parseObject(JSON.toJSONString(map), MaintenanceInfo.class);
        maintenanceInfo1.setHANDLEREMARK("已处理");
        System.out.println(maintenanceInfo1.getHANDLER());
        String text = maintenanceInfo1.getHANDLER();
        JSONArray jsonArray = JSONArray.parseArray(text);
        StringBuilder result = new StringBuilder();
        for (int i=0;i<jsonArray.size();i++){
            result.append(jsonArray.get(i));
            result.append(",");
        }
        maintenanceInfo1.setHANDLER(result.toString().substring(0,result.length()-1));
    if (CollectionUtils.isEmpty(fileIds)){
        iMaintenanceService.update(maintenanceInfo1);
       }else {
        iMaintenanceService.update(maintenanceInfo1,fileIds);
        }
    return ResponseUtil.general_response("success update employee!");
}
    //
    // 删除
    @DeleteMapping("/deleteMaintenance/{id}")
    public Object deleteStationInfo(@PathVariable("id") Integer id) {
        System.out.println("id:"+id);
        iMaintenanceService.delete1(id);
        return ResponseUtil.general_response("success delete employee!");
    }

    @RequestMapping(value = "/uploadDocs",produces="application/json;charset=utf-8")
    @ResponseBody
    public Map<String,Object> uploadDocs(MultipartFile file,@RequestParam(value = "pType", required = false) String pType) {
        Map<String, Object> uploadData = new HashMap<String, Object>();
        String uploadDocsPath = "D:\\images";
        String fileName = file.getOriginalFilename();
        System.out.println("上传的文件名字："+fileName);
        System.out.println(uploadDocsPath+"\\"+fileName);
        File dir = new File(uploadDocsPath, fileName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            file.transferTo(dir);
            cn.geek51.domain.File record = new cn.geek51.domain.File();
            record.setFILENAME(fileName);
            record.setFILEPATH(dir.getPath());
            if (StringUtils.isNoneEmpty(fileName.split("[.]")[fileName.split("[.]").length-1])) {
                record.setFILETYPE(fileName.split("[.]")[fileName.split("[.]").length-1]);
            }
            if (StringUtils.isNoneEmpty(pType)){
                record.setPTYPE(pType);   //设置页面id
            }
            //保存文件列表
            int id = ifileService.saveSelective(record);
            uploadData.put("code", 0);
            uploadData.put("msg", "上传成功");
            uploadData.put("data", id);
        } catch (IOException e) {
            uploadData.put("code", -1);
            uploadData.put("msg", "上传失败");
            uploadData.put("data", "");
        }
        return uploadData;
    }
    @RequestMapping(value = "/uploadDocs2",produces="application/json;charset=utf-8")
    @ResponseBody
    public Map<String,Object> uploadDocs2(MultipartFile file,@RequestParam(value = "pType", required = false) String pType) {
        Map<String, Object> uploadData = new HashMap<String, Object>();
        Date date = new Date();
        String dateStr="";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateStr = simpleDateFormat.format(date);
        String uploadDocsPath = "D:\\yunwei\\guzhangfile\\"+ dateStr;
        String fileName = file.getOriginalFilename();
        System.out.println("上传的文件名字："+fileName);
        System.out.println(uploadDocsPath+"\\"+fileName);
        File dir = new File(uploadDocsPath, fileName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            file.transferTo(dir);
            cn.geek51.domain.File record = new cn.geek51.domain.File();
            record.setFILENAME(fileName);
            record.setFILEPATH(dir.getPath());
            if (StringUtils.isNoneEmpty(fileName.split("[.]")[fileName.split("[.]").length-1])) {
                record.setFILETYPE(fileName.split("[.]")[fileName.split("[.]").length-1]);
            }
            if (StringUtils.isNoneEmpty(pType)){
                record.setPTYPE(pType);   //设置页面id
            }
            //保存文件列表
            int id = ifileService.saveSelective(record);
            uploadData.put("code", 0);
            uploadData.put("msg", "上传成功");
            uploadData.put("data", id);
        } catch (IOException e) {
            uploadData.put("code", -1);
            uploadData.put("msg", "上传失败");
            uploadData.put("data", "");
        }
        return uploadData;
    }
    @RequestMapping(value = "/uploadDocs1",produces="application/json;charset=utf-8")
    @ResponseBody
    public Map<String,Object> uploadDocs1(MultipartFile file,@RequestParam(value="pType",required = false) String pType) {
        Map<String, Object> uploadData = new HashMap<String, Object>();
        Date date = new Date();
        String dateStr="";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateStr = simpleDateFormat.format(date);
        String uploadDocsPath = "D:\\yunwei\\chulifile\\"+dateStr;
        String fileName = file.getOriginalFilename();
        System.out.println("上传的文件名字："+fileName);
        System.out.println(uploadDocsPath+"\\"+fileName);
        File dir = new File(uploadDocsPath, fileName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            file.transferTo(dir);
            cn.geek51.domain.File record = new cn.geek51.domain.File();
            record.setFILENAME(fileName);
            record.setFILEPATH(dir.getPath());
            if (StringUtils.isNoneEmpty(fileName.split("[.]")[fileName.split("[.]").length-1])) {
                record.setFILETYPE(fileName.split("[.]")[fileName.split("[.]").length-1]);
            }
            if (StringUtils.isNoneEmpty(pType)){
                record.setPTYPE(pType);   //设置页面id
            }
            // 保存文件列表
            int id = ifileService.saveSelective(record);
            uploadData.put("code", 0);
            uploadData.put("msg", "上传成功");
            uploadData.put("data", id);
        } catch (IOException e) {
            uploadData.put("code", -1);
            uploadData.put("msg", "上传失败");
            uploadData.put("data", "");
        }
        return uploadData;
    }



    @ResponseBody
    @RequestMapping("/upload1")
    public Map upload1(MultipartFile file, @RequestParam(value = "pType",required = false) String pType){

        String prefix="";
        String dateStr="";
        //保存上传
        OutputStream out = null;
        InputStream fileInput=null;
        try{
            if(file!=null){
                String originalName = file.getOriginalFilename();
                prefix=originalName.substring(originalName.lastIndexOf(".")+1);
                Date date = new Date();
                String uuid = UUID.randomUUID()+"";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateStr = simpleDateFormat.format(date);
                String filepath = "D:\\mycode\\LayUiTest\\src\\main\\resources\\static\\images1\\" + dateStr+"\\"+uuid+"." + prefix;
                File files=new File(filepath);
                //打印查看上传路径
                System.out.println(filepath);
                if(!files.getParentFile().exists()){
                    files.getParentFile().mkdirs();
                }
                file.transferTo(files);
                cn.geek51.domain.File record = new cn.geek51.domain.File();
                record.setFILENAME(originalName);
                record.setFILEPATH(filepath);
                if (StringUtils.isNoneEmpty(prefix)) {
                    record.setFILETYPE(prefix);
                }
                if (StringUtils.isNoneEmpty(pType)){
                    record.setPTYPE(pType);   //设置页面id
                }
                // 保存文件列表
                int id = ifileService.saveSelective(record);
                Map<String,Object> map2=new HashMap<>();
                Map<String,Object> map=new HashMap<>();
                map.put("code",0);
                map.put("msg","");
                map2.put("src","/images/"+ dateStr+"/"+uuid+"." + prefix);
                map2.put("fileId",id);
                map.put("data",map2);
                return map;
            }

        }catch (Exception e){
        }finally{
            try {
                if(out!=null){
                    out.close();
                }
                if(fileInput!=null){
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        Map<String,Object> map=new HashMap<>();
        map.put("code",1);
        map.put("msg","");
        return map;
    }


}
