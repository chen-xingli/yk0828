package cn.geek51.controller;

import cn.geek51.domain.*;
import cn.geek51.service.*;
import cn.geek51.util.ResponseUtil;
import cn.geek51.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 页面转发控制器
 */
@Controller
public class RedirectController {
    @Autowired
    IPositionService positionService;

    @Autowired
    IStationInfoService iStationInfoService;

    @Autowired
    IDepartmentService departmentService;

    @Autowired
    IDeviceinfoService iDeviceinfoService;
    @Autowired
    IMaintenanceService iMaintenanceService;


    @GetMapping("/login")
    public String toLogin() {
        return "user/login";
    }

    @GetMapping("/register")
    public String toRegister() {
        return "user/register";
    }

    @GetMapping("/employee")
    public String toEmployee(Model model) {
        List<Position> positionList = positionService.listAll();
        List<Department> departmentList = departmentService.listAll();
        model.addAttribute("positionList", positionList);
        model.addAttribute("departmentList", departmentList);
        return "employee_view";
    }

    @GetMapping("/imformations")
    public String toImformation(Model model) {
        return "yk_imformation";
    }

    @GetMapping("/stations")
    public String toStations(Model model) {
        return "yk_stations";
    }

    @GetMapping("/inode")
    public String toInode(Model model) {
        return "inode";
    }

    @GetMapping("/maintenance")
    public String toMaintenance(Model model) {
        return "yk_maintenance";
    }


    @GetMapping("/welcome")
    public String toImWelcome(Model model) {

        return "welcome_1";
    }

    @GetMapping("/inspects")
    public String toInspect(Model model) {

        return "yk_inspect";
    }


    @GetMapping("/layers/employee/insert")
    public String toEmployeeInsert(Model model) {
        List<Position> positionList = positionService.listAll();
        List<Department> departmentList = departmentService.listAll();
        model.addAttribute("positionList", positionList);
        model.addAttribute("departmentList", departmentList);
        return "layers/employee_insert";
    }

    // 查询点位数量
    @RequestMapping("/getcount")
    public String getCount(Model model) {
        //获取点位总数量
        int stationcount = iStationInfoService.count();
        //获取设备总数量
        int devicecount = iDeviceinfoService.count();
        model.addAttribute("stationcount", stationcount);
        model.addAttribute("devicecount", devicecount);
        return "welcome_1";
    }

    @GetMapping("/post")
    public String toPost(Model model) {
        UserAuth user = UserContext.getCurrentUser();
        model.addAttribute("user", user);
        return "post_view";
    }

    @GetMapping("/download")
    public String toDownload(Model model) {
        UserAuth user = UserContext.getCurrentUser();
        model.addAttribute("user", user);
        return "download_view";
    }

    @GetMapping("/department")
    public String toDepartment() {
        return "department_view";
    }

    @GetMapping("/auth")
    public String toAuth() {
        return "auth_view";
    }

    @GetMapping("/position")
    public String toPosition() {
        return "position_view";
    }

    @GetMapping("/index")
    public String toIndex(Model model) {
        UserAuth user = UserContext.getCurrentUser();
        model.addAttribute("user", user);
        return "index_view";
    }

    @GetMapping("/")
    public String toMain(Model model) {
        UserAuth user = UserContext.getCurrentUser();
        System.out.println("user" + user);
        model.addAttribute("user", user);
        return "index_view";
    }
    @GetMapping("/showUser")
    @ResponseBody
    public Object showUser(Model model){
        UserAuth user = UserContext.getCurrentUser();
        return user;
    }

    @GetMapping("/showbyid/{id}")
    @ResponseBody
    public Object showMaintenanceById(@PathVariable("id") Integer id){
        MaintenanceInfo maintenanceInfo = iMaintenanceService.listOneById(id);
        System.err.println(maintenanceInfo);
        return maintenanceInfo;
    }

}
