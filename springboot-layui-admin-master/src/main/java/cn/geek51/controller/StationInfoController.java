package cn.geek51.controller;

import cn.geek51.domain.*;
import cn.geek51.service.IDeviceinfoService;
import cn.geek51.service.IStationInfoService;
import cn.geek51.service.InspectService;
import cn.geek51.util.ResponseUtil;
import cn.geek51.util.UserContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.io.File;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.poi.ss.usermodel.CellType.STRING;

@RestController
public class StationInfoController {
    @Autowired
    IStationInfoService iStationInfoService;
    @Autowired
    InspectService inspectService;
    @Autowired
    IDeviceinfoService iDeviceinfoService;

    @PostMapping("/insertStation")
    public Object insertStationInfo(StationInfo stationInfo) {
        System.out.println("新增");
        System.out.println(stationInfo);
        stationInfo.setAREA(UserContext.getCurrentUser().getArea());
        iStationInfoService.save(stationInfo);
        System.out.println(stationInfo.getId());
        Inspect inspect = new Inspect();
        inspect.setSTATIONID(stationInfo.getId().toString());
//        inspect.setSTATUS(1);

        inspectService.save(inspect);
        return ResponseUtil.general_response("success insert stationInfo!");
    }

    // 查询, query传入LAYUI查询参数
    @GetMapping("/station")
    public Object getStationInfo(PageHelper pageHelper, String query) throws Exception {
        Map<Object, Object> map = pageHelper.getMap();
        Map<Object, Object> maps = pageHelper.getMap();
        HashMap queryMap = null;
        //进行拼接, 拼接成一个MAP查询
        if (query != null) {
            queryMap = new ObjectMapper().readValue(query, HashMap.class);
            map.putAll(queryMap);
        }
        UserAuth user = UserContext.getCurrentUser();
        map.put("Area",user.getArea());
        //条数查询
        maps.put("Area",user.getArea());
        List<StationInfo> deviceinfoList = iStationInfoService.listAll(map);
        // map重用, 用于回传pagesize asd as das as d sa as  h
        if (map == null) map = new HashMap<>();
        else map.clear();
        map.put("size", iStationInfoService.counts(maps));
        return ResponseUtil.general_response(deviceinfoList, map);
    }

    //    @GetMapping("/imformation/{id}")
//    public Object getEmployee(@PathVariable("id") Integer id) {
//        Employee employee = employeeService.listOneById(id);
//        return ResponseUtil.general_response(employee);
//    }
//
    @GetMapping("/getStationById/{id}")
    public Object getStationById(@PathVariable("id") Integer id) {
        StationInfo stationInfo = iStationInfoService.listOneById(id);
        System.err.println(stationInfo);
        return ResponseUtil.general_response(stationInfo);
    }
    @GetMapping("/getdeviceTypeById/{id}")
    public Object getDeviceById(@PathVariable("id") Integer id) {
        Deviceinfo deviceinfo = iDeviceinfoService.listByStId(Integer.toString(id));
        System.err.println(deviceinfo);
        return ResponseUtil.general_response(deviceinfo);
    }
    //更改
    @PutMapping("/updateStation")
    public Object updateStationInfo(@RequestBody Map<String, String> map) {
        StationInfo stationInfo = new StationInfo();
        //System.out.println("ststionid"+map.get("ststionid"));
        System.out.println(map.get("ID"));
        System.out.println(map.get("Remark"));
        System.out.println(map.get("Cycle"));
        stationInfo.setId(Integer.parseInt(map.get("ID")));
//        stationInfo.setStationName(map.get("stationname"));
//        stationInfo.setDirection(map.get("direction"));
//        stationInfo.setAreaName(map.get("areaname"));
//        stationInfo.setRoadCode(map.get("roadcode"));
//        stationInfo.setRoadName(map.get("roadname"));
//        stationInfo.setStakeNumber(map.get("stakenumber"));
//        stationInfo.setLng(map.get("lng"));
//        stationInfo.setLat(map.get("lat"));
//        stationInfo.setDeviceCode(map.get("devicecode"));
//        stationInfo.setLaneNumber(Integer.parseInt(map.get("lanenumber")));
//        stationInfo.setCreateTime(Timestamp.valueOf(map.get("createtime")));
//        stationInfo.setCycle(map.get("cycle"));
        stationInfo.setCycle(map.get("Cycle"));
        stationInfo.setRemark(map.get("Remark"));
        iStationInfoService.update(stationInfo);
        return ResponseUtil.general_response("success update employee!");
    }

    //
    // 删除
    @DeleteMapping("/deletestation/{id}")
    public Object deleteStationInfo(@PathVariable("id") Integer id) {
        iStationInfoService.delete(id);
        String STATIONID = id.toString();
        inspectService.delete1(STATIONID);
        return ResponseUtil.general_response("success delete employee!");
    }

    @GetMapping("/selectIdName")
    public List<StationInfo> selectIdName(Model model) {
        List<StationInfo> stationInfos = iStationInfoService.listIdName(null);
        return stationInfos;
    }

    @GetMapping("/getll")
    public List<StationInfo> getll() {
        List<StationInfo> stationInfo = iStationInfoService.listLLAll(null);
        return stationInfo;
    }

    @PostMapping("/uploadByExcel")
    public Object uploadByExcel(MultipartFile file) throws IOException {
        System.out.println("用excel导入多个点位信息");
        InputStream in = file.getInputStream();
        //
        Workbook wb = new HSSFWorkbook(in);
        Sheet sheet = wb.getSheetAt(0);
        int rows = sheet.getLastRowNum();
        System.out.println("准备导入记录数：" + rows);
        List<StationInfo> stationInfos = new ArrayList<>();
        for (int i = 1; i <= rows; i++) {
            StationInfo stationInfo = new StationInfo();
            Row row = sheet.getRow(i);
            for (int j = 0; j < row.getLastCellNum(); j++) {
                Cell cell = row.getCell(j);
                if (cell != null) {
                    cell.setCellType(STRING);
                    switch (j) {
                        case 0:
                            stationInfo.setStationName(cell.getStringCellValue());
                            break;
                        case 1:
                            stationInfo.setDirection(cell.getStringCellValue());
                            break;
                        case 2:
                            stationInfo.setAreaName(cell.getStringCellValue());
                            break;
                        case 3:
                            stationInfo.setRoadCode(cell.getStringCellValue());
                            break;
                        case 4:
                            stationInfo.setRoadName(cell.getStringCellValue());
                            break;
                        case 5:
                            stationInfo.setStakeNumber(cell.getStringCellValue());
                            break;
                        case 6:
                            stationInfo.setLng(cell.getStringCellValue());
                            break;
                        case 7:
                            stationInfo.setLat(cell.getStringCellValue());
                            break;
                        case 8:
                            stationInfo.setDeviceCode(cell.getStringCellValue());
                            break;
                        case 9:
                            stationInfo.setLaneNumber(cell.getStringCellValue());
                            break;


                        case 10:
//
                            if (cell.getStringCellValue()==null||cell.getStringCellValue()==""||cell.getStringCellValue().equals("")){
                                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                                Date date = new Date();
                                String s = date.toString();
                                try {
                                    date=df.parse(s);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Timestamp timestamp = new Timestamp(date.getTime());
                                System.err.println("aaa"+timestamp);
                                stationInfo.setCreateTime(timestamp);
                            }else {
                                String dateStr = cell.getStringCellValue();
                                DateFormat format = new SimpleDateFormat("yyyyMMdd");//日期格式
                                Date date = null;
                                try {
                                    date = format.parse(dateStr);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Timestamp time = new Timestamp(date.getTime());
                                stationInfo.setCreateTime(time);
                            }
                            break;
                        case 11:
                            stationInfo.setCycle(cell.getStringCellValue());
                            break;
                        case 12:
                            stationInfo.setRemark(cell.getStringCellValue());
                            break;
                        case 13:
                            stationInfo.setStationShortName(cell.getStringCellValue());
                            break;
                        case 14:
                            stationInfo.setPROJECTNAME(cell.getStringCellValue());
                            break;
                        case 15:
                            stationInfo.setINSTALLTYPE(cell.getStringCellValue());
                            break;
                        case 16:
                            stationInfo.setINSTALLPLACE(cell.getStringCellValue());
                            break;
                        case 17:
                            stationInfo.setDEVICETYPE(cell.getStringCellValue());
                            break;
                        case 18:
                            stationInfo.setPIXEL(cell.getStringCellValue());
                            break;
                        case 19:
                            stationInfo.setPOWERTYPE(cell.getStringCellValue());
                            break;
                        case 20:
                            stationInfo.setTELPHONE(cell.getStringCellValue());
                            break;
                        case 21:
                            stationInfo.setWORKTYPE(cell.getStringCellValue());
                            break;
                        case 22:
                            stationInfo.setBUILTLENGTH(cell.getStringCellValue());
                            break;
                        case 23:
                            stationInfo.setPOWERLENGTH(cell.getStringCellValue());
                            break;
                        case 24:
                            stationInfo.setNETWORK(cell.getStringCellValue());
                            break;
                        case 25:
                            stationInfo.setAREA(cell.getStringCellValue());
                            break;
                        case 26:
                            if (cell.getStringCellValue()==null||cell.getStringCellValue()==""||cell.getStringCellValue().equals("")){
                                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                                Date date = new Date();
                                String s = date.toString();
                                try {
                                    date=df.parse(s);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Timestamp timestamp = new Timestamp(date.getTime());
                                System.err.println("aaa"+timestamp);
                                stationInfo.setFIRSTTIME(timestamp);
                            }else {
                                String dateStr = cell.getStringCellValue();
                                DateFormat format = new SimpleDateFormat("yyyyMMdd");//日期格式
                                Date date = null;
                                try {
                                    date = format.parse(dateStr);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Timestamp time = new Timestamp(date.getTime());
                                stationInfo.setFIRSTTIME(time);
                            }
                            break;
                    }
                }
            }
            System.out.println(stationInfo);
            Integer id = stationInfo.getId();
            stationInfos.add(stationInfo);
        }
        int n = 0;
        String msg = "";
        try{
            n = iStationInfoService.insertStationList(stationInfos);
            System.err.println(n);
            Inspect inspect = new Inspect();
            Integer id = stationInfos.get(0).getId();
            for (int i=id;i>id-n;i--){
                inspect.setSTATIONID(Integer.toString(i));
                //把这句加上就行。
                inspect.setSTATUS("1");
                inspect.setINSPECTDATE(stationInfos.get(0).getFIRSTTIME());
                inspect.setAREA(stationInfos.get(0).getAREA());
                inspectService.save(inspect);
                System.err.println("执行了i"+i+"次");
            }
            System.err.println("id"+id);
        }catch (Exception e){
            msg = "error";
        }
        msg = "成功导入" + n + "条点位信息";
        return ResponseUtil.general_response(msg);
    }

    @RequestMapping("/addpic/{id}")
    @ResponseBody
    public Object addpic(@PathVariable("id") Integer id, MultipartFile file) throws IOException {

        String dateStr = "";
        //保存上传
        OutputStream out = null;
        InputStream fileInput = null;
        try {
            if (file != null) {
                String originalName = file.getOriginalFilename();
//                prefix = originalName.substring(originalName.lastIndexOf(".") + 1);
                Date date = new Date();
                String uuid = UUID.randomUUID() + "";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy年M月d日H时");
                dateStr = simpleDateFormat.format(date);
                File file1 = new File("D:\\StationPic");//总站点照片文件夹
                if (!file1.exists()){
                    file1.mkdirs();
                    File file2 = new File("D:\\StationPic\\"  + simpleDateFormat2.format(date));
                    if (!file2.exists()){
                        file2.mkdirs();
                    }
                }
                String filepath = "D:\\stationpic\\" +simpleDateFormat2.format(date)+"\\"+ dateStr + uuid.substring(0,8) + "-" +originalName;
                File files = new File(filepath);
                //打印查看上传路径
                System.out.println(filepath);
                if (!files.getParentFile().exists()) {
                    files.getParentFile().mkdirs();
                }
                file.transferTo(files);
                StationInfo stationInfo = iStationInfoService.listOneById(id);
                System.out.println("之前的"+stationInfo.getFILE());
                if (stationInfo.getFILE()==null || "".equals(stationInfo.getFILE())){
                    stationInfo.setFILE(filepath);
                }else {
                    stationInfo.setFILE(stationInfo.getFILE()+","+ filepath);
                }
                iStationInfoService.updateFile(stationInfo);
                cn.geek51.domain.File record = new cn.geek51.domain.File();
                record.setFILENAME(originalName);
                record.setFILEPATH(filepath);
                if (StringUtils.isNoneEmpty(originalName)) {
                    record.setFILETYPE(originalName);
                }
            }
        } catch (Exception e) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("msg", "");
        return map;
    }
    @GetMapping("/stationExcelModelDownload")
    public void stationExcelModelDownload(HttpServletResponse response) {
        // 服务器保存的文件地址，即你要下载的文件地址（全路径）
        System.out.println("开始导出excel");
        String path = "D:\\点位信息导入模板\\点位信息导入模板.xls";
        String fileName = "点位信息导入模板.xls";
        File file = new File(path);
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(file));
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            response.reset();
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            response.addHeader("Content-Length", "" + file.length());
            response.setContentType("application/octet-stream");
            outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(buffer);
            outputStream.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @GetMapping("/IoReadImage/{imgPath1}/{imgPath2}/{imgPath3}/{imgPath4}")
    public String IoReadImage(@PathVariable String imgPath1,@PathVariable String imgPath2,@PathVariable String imgPath3,@PathVariable String imgPath4, HttpServletResponse response) throws IOException {
        ServletOutputStream out = null;
        FileInputStream ips = null;
        String filePath = imgPath1+"/"+imgPath2+"/"+imgPath3+"/"+imgPath4+".jpg";
        System.out.println("本地路径"+filePath);
        try {
            //获取图片存放路径
            ips = new FileInputStream(new File(filePath));
            response.setContentType("multipart/form-data");
            out = response.getOutputStream();
            //读取文件流
            int len = 0;
            byte[] buffer = new byte[1024 * 10];
            while ((len = ips.read(buffer)) != -1){
                out.write(buffer,0,len);
            }
            out.flush();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            out.close();
            ips.close();
        }
        return null;
    }
}
