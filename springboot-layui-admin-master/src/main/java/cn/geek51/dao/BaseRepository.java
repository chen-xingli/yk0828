package cn.geek51.dao;

import cn.geek51.domain.Post;
import com.sun.org.apache.xml.internal.utils.NameSpace;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 持久层基类 - 抽象模版设计模式
 */
@Repository
public class BaseRepository<T> {
    private String nameSpace = this.getClass().getName() + ".";

    public String getClassName() {
        Class<T> tClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return tClass.getName();
    }
    @Autowired
    protected SqlSessionTemplate sqlSessionTemplate;

    /*插入数据*/
    public int insertSelective(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.insert(nameSpace + methodName, param);
    }
    public int insertSelective1(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.insert(nameSpace + methodName, param);
    }
    public int insertDomainList(Object param,String nameSpace){
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.insert(nameSpace + methodName, param);
    }
    public int insertStationPic(Object param,String nameSpace){
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.insert(nameSpace + methodName, param);
    }

    /*插入数据*/
    public int upinsertSelective(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.insert(nameSpace + methodName, param);
    }

    /**
     * 根据给定参数删除数据
     * @param param
     * @return 执行返回值
     */
    public int deleteOneByParams(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.delete(nameSpace + methodName, param);
    }
    public int deleteOneByID(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.delete(nameSpace + methodName, param);
    }

    /**
     * 查询数据
     */
    public T selectOneByPrimaryKey(Object id, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectOne(nameSpace + methodName, id);
    }
    public T selectByStId(Object StId, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectOne(nameSpace + methodName, StId);
    }
    public List<T> selectAllByParams(Object param, String namespace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectList(namespace + methodName, param);
    }

    public List<T> listIdAndName(String namespace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectList(namespace + methodName);
    }

    public List<T> selectFile(Object id, String namespace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectList(namespace + methodName, id);
    }

    public List<T> selectMain(Object id, String namespace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectList(namespace + methodName, id);
    }
    public List<T> selectllByParams(Object param, String namespace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectList(namespace + methodName, param);
    }
    public List<T> selectIdNameByParams(Object param, String namespace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectList(namespace + methodName, param);
    }


    public T selectNameByPrimaryKey(Object id, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectOne(nameSpace + methodName, id);
    }

    /**
     * 更新数据
     * 可选参数
     */
    public int updateByPrimaryKeySelective(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.update(nameSpace + methodName, param);
    }
    public int updateStationFile(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.update(nameSpace + methodName, param);
    }

    public int getCount(String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectOne(nameSpace + methodName);
    }

    public int counts(Object param, String nameSpace) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        return sqlSessionTemplate.selectOne(nameSpace + methodName, param);
    }

}
