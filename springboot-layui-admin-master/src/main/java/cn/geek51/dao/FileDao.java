package cn.geek51.dao;

import cn.geek51.domain.File;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FileDao extends BaseRepository{

    private final String namespace = "cn.geek51.domain.File.";

    /**
     * 保存文件上传信息
     * @param file
     * @return
     */
    public int insertSelective1(File file){
        insertSelective1(file, namespace);
        return file.getID();
    }

    public int updateByPrimaryKeySelective(File files){
        return updateByPrimaryKeySelective(files,namespace);
    }
}
