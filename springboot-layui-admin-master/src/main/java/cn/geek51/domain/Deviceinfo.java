package cn.geek51.domain;

import lombok.ToString;

@ToString
public class Deviceinfo {
    private Integer    ID;
    private  String    STSTIONID;
    private String     STATIONIDNAME;
    private  String    DEVICENAME;
    private  String    DEVICEBRAND;
    private  String    DEVICEMODEL;
    private  String    DEVICEVERSION;
    private  String    IP;
    private  String    User;
    private  String    Psw;
    private  String    REMARK;
    private  String    Online;
    private String     Area;

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getOnline() {
        return Online;
    }

    public void setOnline(String online) {
        Online = online;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getSTSTIONID() {
        return STSTIONID;
    }

    public void setSTSTIONID(String STSTIONID) {
        this.STSTIONID = STSTIONID;
    }

    public String getDEVICENAME() {
        return DEVICENAME;
    }

    public void setDEVICENAME(String DEVICENAME) {
        this.DEVICENAME = DEVICENAME;
    }

    public String getDEVICEBRAND() {
        return DEVICEBRAND;
    }

    public void setDEVICEBRAND(String DEVICEBRAND) {
        this.DEVICEBRAND = DEVICEBRAND;
    }

    public String getDEVICEMODEL() {
        return DEVICEMODEL;
    }

    public void setDEVICEMODEL(String DEVICEMODEL) {
        this.DEVICEMODEL = DEVICEMODEL;
    }

    public String getDEVICEVERSION() {
        return DEVICEVERSION;
    }

    public void setDEVICEVERSION(String DEVICEVERSION) {
        this.DEVICEVERSION = DEVICEVERSION;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getPsw() {
        return Psw;
    }

    public void setPsw(String psw) {
        Psw = psw;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public String getSTATIONIDNAME() {
        return STATIONIDNAME;
    }

    public void setSTATIONIDNAME(String STATIONIDNAME) {
        this.STATIONIDNAME = STATIONIDNAME;
    }
}
