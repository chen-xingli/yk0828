package cn.geek51.domain;

import lombok.ToString;

@ToString
public class File {
    private  int ID;
    private  String FILEID;
    private String FILENAME;
    private String FILEPATH;
    private String FILETYPE;
    private String INSPECTID;
    private String MAINTENANCEID;
    private String PTYPE;   //页面id

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFILEID() {
        return FILEID;
    }

    public void setFILEID(String FILEID) {
        this.FILEID = FILEID;
    }

    public String getFILENAME() {
        return FILENAME;
    }

    public void setFILENAME(String FILENAME) {
        this.FILENAME = FILENAME;
    }

    public String getFILEPATH() {
        return FILEPATH;
    }

    public void setFILEPATH(String FILEPATH) {
        this.FILEPATH = FILEPATH;
    }

    public String getFILETYPE() {
        return FILETYPE;
    }

    public void setFILETYPE(String FILETYPE) {
        this.FILETYPE = FILETYPE;
    }

    public String getMAINTENANCEID() {
        return MAINTENANCEID;
    }

    public void setMAINTENANCEID(String MAINTENANCEID) {
        this.MAINTENANCEID = MAINTENANCEID;
    }

    public String getINSPECTID() {
        return INSPECTID;
    }

    public void setINSPECTID(String INSPECTID) {
        this.INSPECTID = INSPECTID;
    }

    public String getPTYPE() {
        return PTYPE;
    }

    public void setPTYPE(String PTYPE) {
        this.PTYPE = PTYPE;
    }
}
