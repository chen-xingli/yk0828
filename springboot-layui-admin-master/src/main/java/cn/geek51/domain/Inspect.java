package cn.geek51.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Map;

@ToString
public class Inspect {

    //0 表示成功 1表示失败
    private Integer code;
    //信息
    private String msg;
    //url
    private Map<String, String> data;
    private Integer totalCount=0;
    private Integer successCount=0;
    private Integer failCount=0;
    private int ID;
    private String STATUS;
    private String STATIONID;
    private String STATIONIDNAME;
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date INSPECTDATE;
    private String INSPECTER;
    private String INSPECTREMARK;
    private String INSPECTPHOTO;
    private String AREA;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getSTATIONID() {
        return STATIONID;
    }

    public void setSTATIONID(String STATIONID) {
        this.STATIONID = STATIONID;
    }

    public Date getINSPECTDATE() {
        return INSPECTDATE;
    }

    public void setINSPECTDATE(Date INSPECTDATE) {
        this.INSPECTDATE = INSPECTDATE;
    }

    public String getINSPECTER() {
        return INSPECTER;
    }

    public void setINSPECTER(String INSPECTER) {
        this.INSPECTER = INSPECTER;
    }

    public String getINSPECTREMARK() {
        return INSPECTREMARK;
    }

    public void setINSPECTREMARK(String INSPECTREMARK) {
        this.INSPECTREMARK = INSPECTREMARK;
    }

    public String getINSPECTPHOTO() {
        return INSPECTPHOTO;
    }

    public void setINSPECTPHOTO(String INSPECTPHOTO) {
        this.INSPECTPHOTO = INSPECTPHOTO;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATIONIDNAME() {
        return STATIONIDNAME;
    }

    public void setSTATIONIDNAME(String STATIONIDNAME) {
        this.STATIONIDNAME = STATIONIDNAME;
    }

    public String getAREA() {
        return AREA;
    }

    public void setAREA(String AREA) {
        this.AREA = AREA;
    }
}

