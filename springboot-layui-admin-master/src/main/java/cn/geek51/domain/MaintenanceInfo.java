package cn.geek51.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor

public class MaintenanceInfo {
    private Integer ID;
    private String STATIONID;
    private String DEVICEID;
    private String DESCRIPTION;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Timestamp SUBMITTIME;
    private String SUBMITTER;
    private String SUBMITPHOTO;
    private String HANDLEREMARK;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Timestamp HANDLETIME;
    private String HANDLER;
    private String HANDLEPHOTO;
    private String REMARK;
    private String AREA;

    public String getAREA() {
        return AREA;
    }

    public void setAREA(String AREA) {
        this.AREA = AREA;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getSTATIONID() {
        return STATIONID;
    }

    public void setSTATIONID(String STATIONID) {
        this.STATIONID = STATIONID;
    }

    public String getDEVICEID() {
        return DEVICEID;
    }

    public void setDEVICEID(String DEVICEID) {
        this.DEVICEID = DEVICEID;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public Date getSUBMITTIME() {
        return SUBMITTIME;
    }

    public void setSUBMITTIME(Timestamp SUBMITTIME) {
        this.SUBMITTIME = SUBMITTIME;
    }

    public String getSUBMITTER() {
        return SUBMITTER;
    }

    public void setSUBMITTER(String SUBMITTER) {
        this.SUBMITTER = SUBMITTER;
    }

    public String getSUBMITPHOTO() {
        return SUBMITPHOTO;
    }

    public void setSUBMITPHOTO(String SUBMITPHOTO) {
        this.SUBMITPHOTO = SUBMITPHOTO;
    }

    public String getHANDLEREMARK() {
        return HANDLEREMARK;
    }

    public void setHANDLEREMARK(String HANDLEREMARK) {
        this.HANDLEREMARK = HANDLEREMARK;
    }

    public Date getHANDLETIME() {
        return HANDLETIME;
    }

    public void setHANDLETIME(Timestamp HANDLETIME) {
        this.HANDLETIME = HANDLETIME;
    }

    public String getHANDLER() {
        return HANDLER;
    }

    public void setHANDLER(String HANDLER) {
        this.HANDLER = HANDLER;
    }

    public String getHANDLEPHOTO() {
        return HANDLEPHOTO;
    }

    public void setHANDLEPHOTO(String HANDLEPHOTO) {
        this.HANDLEPHOTO = HANDLEPHOTO;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }
}
