package cn.geek51.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.sql.Timestamp;


@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StationInfo implements Serializable {
    private Integer ID;
    private String StationName;//点位名称
    private String StationShortName;//点位朝向
    private String Direction;//路线朝向
    private String AreaName;
    private String RoadCode;
    private String RoadName;
    private String StakeNumber;
    private String Lng;
    private String Lat;
    private String DeviceCode;
    private String LaneNumber;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Timestamp CreateTime;
    private String Cycle;
    private String Remark;
    private String PROJECTNAME;
    private String INSTALLTYPE;
    private String INSTALLPLACE;
    private String DEVICETYPE;
    private String PIXEL;
    private String POWERTYPE;
    private String TELPHONE;
    private String WORKTYPE;
    private String BUILTLENGTH;
    private String POWERLENGTH;
    private String NETWORK;
    private String FILE;
    private String AREA;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Timestamp FIRSTTIME;

    public Timestamp getFIRSTTIME() {
        return FIRSTTIME;
    }

    public void setFIRSTTIME(Timestamp FIRSTTIME) {
        this.FIRSTTIME = FIRSTTIME;
    }

    public String getAREA() {
        return AREA;
    }

    public void setAREA(String AREA) {
        this.AREA = AREA;
    }

    public Integer getId() {
        return ID;
    }

    public void setId(Integer id) {
        this.ID = id;
    }

    public String getStationName() {
        return StationName;
    }

    public void setStationName(String stationName) {
        StationName = stationName;
    }

    public String getStationShortName() {
        return StationShortName;
    }

    public void setStationShortName(String stationShortName) {
        StationShortName = stationShortName;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        AreaName = areaName;
    }

    public String getRoadCode() {
        return RoadCode;
    }

    public void setRoadCode(String roadCode) {
        RoadCode = roadCode;
    }

    public String getRoadName() {
        return RoadName;
    }

    public void setRoadName(String roadName) {
        RoadName = roadName;
    }

    public String getStakeNumber() {
        return StakeNumber;
    }

    public void setStakeNumber(String stakeNumber) {
        StakeNumber = stakeNumber;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String lng) {
        Lng = lng;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getDeviceCode() {
        return DeviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        DeviceCode = deviceCode;
    }

    public String getLaneNumber() {
        return LaneNumber;
    }

    public void setLaneNumber(String laneNumber) {
        LaneNumber = laneNumber;
    }

    public Timestamp getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Timestamp createTime) {
        CreateTime = createTime;
    }

    public String getCycle() {
        return Cycle;
    }

    public void setCycle(String cycle) {
        Cycle = cycle;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }
    public String getPROJECTNAME() {
        return PROJECTNAME;
    }

    public void setPROJECTNAME(String PROJECTNAME) {
        this.PROJECTNAME = PROJECTNAME;
    }

    public String getINSTALLTYPE() {
        return INSTALLTYPE;
    }

    public void setINSTALLTYPE(String INSTALLTYPE) {
        this.INSTALLTYPE = INSTALLTYPE;
    }

    public String getINSTALLPLACE() {
        return INSTALLPLACE;
    }

    public void setINSTALLPLACE(String INSTALLPLACE) {
        this.INSTALLPLACE = INSTALLPLACE;
    }

    public String getDEVICETYPE() {
        return DEVICETYPE;
    }

    public void setDEVICETYPE(String DEVICETYPE) {
        this.DEVICETYPE = DEVICETYPE;
    }

    public String getPIXEL() {
        return PIXEL;
    }

    public void setPIXEL(String PIXEL) {
        this.PIXEL = PIXEL;
    }

    public String getPOWERTYPE() {
        return POWERTYPE;
    }

    public void setPOWERTYPE(String POWERTYPE) {
        this.POWERTYPE = POWERTYPE;
    }

    public String getTELPHONE() {
        return TELPHONE;
    }

    public void setTELPHONE(String TELPHONE) {
        this.TELPHONE = TELPHONE;
    }

    public String getWORKTYPE() {
        return WORKTYPE;
    }

    public void setWORKTYPE(String WORKTYPE) {
        this.WORKTYPE = WORKTYPE;
    }

    public String getBUILTLENGTH() {
        return BUILTLENGTH;
    }

    public void setBUILTLENGTH(String BUILTLENGTH) {
        this.BUILTLENGTH = BUILTLENGTH;
    }

    public String getPOWERLENGTH() {
        return POWERLENGTH;
    }

    public void setPOWERLENGTH(String POWERLENGTH) {
        this.POWERLENGTH = POWERLENGTH;
    }

    public String getNETWORK() {
        return NETWORK;
    }

    public void setNETWORK(String NETWORK) {
        this.NETWORK = NETWORK;
    }

    public String getFILE() {
        return FILE;
    }

    public void setFILE(String FILE) {
        this.FILE = FILE;
    }
}
