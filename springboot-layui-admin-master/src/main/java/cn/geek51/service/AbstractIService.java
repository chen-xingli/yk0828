package cn.geek51.service;

import cn.geek51.domain.MaintenanceInfo;

import java.util.List;

public interface AbstractIService<T> {
    /**
     * 增加元素
     * @param object
     */
    int save(T object);
    int insertStationList(Object object);
    int saveMaintenance(MaintenanceInfo maintenanceInfo);
    int upsave(T object);




    /**
     * 查询所有元素
     */
    List<T> listAll();
    List<T> listIdAndName();

    List<T> listAll(Object object);
    List<T> listLLAll(Object object);
    List<T> listIdName(Object object);


    List<T> listfile(Integer id);
    List<T> listmain(Integer id);





    T listOneById(Integer id);
    T listNameById(Integer id);
    T listByStId(String StId);




    /**
     * 更改指定元素
     * @param object
     */
    int update(Object object);
//    int updatepic(Object object);
    int updateFile(Object object);
    /**
     * 根据ID删除指定元素
     * @param object
     */
    int delete(Object object);

    int delete1(Object object);


    /**
     * 获取所有数据的总条数
     * @return
     */
    int count();

    int counts(Object object);



}
