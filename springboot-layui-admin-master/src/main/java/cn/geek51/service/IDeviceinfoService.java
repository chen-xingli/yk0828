package cn.geek51.service;

import cn.geek51.domain.Deviceinfo;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface IDeviceinfoService extends AbstractIService<Deviceinfo> {
    List<Map<String, Object>> getListCommon(@Param(value = "sqlStr") String sqlStr);
}
