package cn.geek51.service;

import cn.geek51.domain.FileUpload;
import cn.geek51.domain.MaintenanceInfo;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IMaintenanceService extends AbstractIService<MaintenanceInfo>{

    /**
     * 保存文件信息
     * @param maintenanceInfo
     * @param fileIds
     * @return
     */
    int save(MaintenanceInfo maintenanceInfo, List<Integer> fileIds);

    /**
     * 运维
     * @param maintenanceInfo
     * @param fileIds
     * @return
     */
    int update(MaintenanceInfo maintenanceInfo, List<Integer> fileIds);
}
