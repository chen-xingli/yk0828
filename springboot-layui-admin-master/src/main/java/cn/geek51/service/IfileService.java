package cn.geek51.service;


import cn.geek51.domain.File;


public interface IfileService extends AbstractIService<File> {

    int saveSelective(File file);
}
