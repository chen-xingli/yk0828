package cn.geek51.service.impl;


import cn.geek51.dao.FileDao;
import cn.geek51.domain.File;
import cn.geek51.service.IfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl extends BaseServiceImpl<File> implements IfileService {

    @Autowired
    private FileDao fileDao;


    @Override
    public int saveSelective(File file) {
        return fileDao.insertSelective1(file);
    }

}
