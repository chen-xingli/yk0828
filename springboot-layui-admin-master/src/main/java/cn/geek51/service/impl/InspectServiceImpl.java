package cn.geek51.service.impl;


import cn.geek51.domain.Inspect;

import cn.geek51.service.InspectService;
import org.springframework.stereotype.Service;

@Service
public class InspectServiceImpl extends BaseServiceImpl<Inspect> implements InspectService {

}
