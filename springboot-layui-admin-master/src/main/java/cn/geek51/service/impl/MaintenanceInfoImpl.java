package cn.geek51.service.impl;

import cn.geek51.dao.FileDao;
import cn.geek51.dao.MaintenanceDao;
import cn.geek51.domain.Deviceinfo;
import cn.geek51.domain.File;
import cn.geek51.domain.MaintenanceInfo;
import cn.geek51.service.IDeviceinfoService;
import cn.geek51.service.IMaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MaintenanceInfoImpl extends BaseServiceImpl<MaintenanceInfo> implements IMaintenanceService {

    @Autowired
    private FileDao fileDao;

    @Override
    public int save(MaintenanceInfo maintenanceInfo, List<Integer> fileIds) {
        refreshActualClassName();
        baseRepository.insertSelective(maintenanceInfo,this.actualClassName);
        String MAINTENANCEID = maintenanceInfo.getID().toString();
        for (Integer fileId:fileIds) {
            File file = new File();
            file.setID(fileId);
            file.setMAINTENANCEID(MAINTENANCEID);
            fileDao.updateByPrimaryKeySelective(file);
        }
        return 0;
    }

    @Override
    public int update(MaintenanceInfo maintenanceInfo, List<Integer> fileIds) {
        refreshActualClassName();
        baseRepository.updateByPrimaryKeySelective(maintenanceInfo, this.actualClassName);
        String MAINTENANCEID = maintenanceInfo.getID().toString();
        for (Integer fileId:fileIds) {
            File file = new File();
            file.setID(fileId);
            file.setMAINTENANCEID(MAINTENANCEID);
            fileDao.updateByPrimaryKeySelective(file);
        }
        return 0;
    }
}
