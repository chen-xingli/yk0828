package cn.geek51.service.impl;

import cn.geek51.domain.StationInfo;
import cn.geek51.service.IStationInfoService;
import org.springframework.stereotype.Service;

@Service
public class StationServiceImpl extends BaseServiceImpl<StationInfo> implements IStationInfoService {

}
