<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="../static/layui/css/modules/yk.css">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<#--		<link rel="stylesheet" href="../lib/layui-v2.5.5/css/layui.css" media="all">-->
		<link rel="stylesheet" href="../static/layui/css/layui.css">
		<#--		<link rel="stylesheet" href="../lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">-->
		<#--		<link rel="stylesheet" href="../css/public.css" media="all">-->
		<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css" media="all">
		<link rel="stylesheet" href="../font-awesome-4.7.0/css/public.css" media="all">
	</head>
	<body>
		<div class="layuimini-container layui-bgcol">
			<div class="layuimini-main">
				<div class="layui-row">
					<div class="layui-float layui-float-left">
						<div class="layui-left-bdcon clear">
							<div class="layui-ele">
								<h2>2020年7月10日</h2>
								<p>09:37:08</p>
							</div>
						</div>
					</div>
					<div class="layui-float layui-float-cen">
						<div class="layui-cen clear">
							<div class="layui-cen-a"><a href="javascript:void(0)">数据监控</a></div>
							<div><h1>通途西路K3+300</h1></div>
							<div class="layui-cen-a"><a href="javascript:void(0)">设备总览</a></div>
						</div>
					</div>
					<div class="layui-float layui-float-right">
						<div class="layui-right-bdcon clear">
							<div class="layui-ele">
								<h2>2020年7月10日</h2>
								<p>09:37:08</p>
							</div>
						</div>
					</div>
				</div>
				<div class="layui-row layui-botton-con">
					<div class="layui-botton-left layui-four-border">
						<div class="layui-botton-leftcon layui-border">
							<h2>点位信息</h2>
							<div class="layui-botton-leftsrc">
								<span>点位名称：</span>
								<input type="text" />
								<a href="javascript:(0)">搜索</a>
							</div>
							<div class="layui-botton-leftree">
								<div id="layui-botton-tree" class="demo-tree-more"></div>
							</div>
						</div>
					</div>
					<div class="layui-botton-cen">
						<div class="layui-cen-topfour layui-four-border">
							<div class="layui-border">
								<div class="layui-cen-tone clear">
									<p class="layui-real-time">实时数据</p>
									<p class="layui-update-date"><label>数据更新时间：</label><span>2020-10-20 15:10:30</span></p>
								</div>
								<div class="layui-cen-ctwo clear">
									<p>设备总数：<span>56</span></p>
									<div class="layui-details-show">
										<div class="layui-equipment-number">
											<div>运行中</div>
											<p>工控机</p>
											<i class="layui-equipment-normal"></i>
										</div>
										<div class="layui-equipment-number">
											<div>异常</div>
											<p>称台</p>
											<i class="layui-equipment-abnormal"></i>
										</div>
										<div class="layui-equipment-number">
											<div>正常</div>
											<p>抓拍机</p>
											<i class="layui-equipment-normal"></i>
										</div>
										<div class="layui-equipment-number">
											<div>正常</div>
											<p>LED显示屏</p>
											<i class="layui-equipment-normal"></i>
										</div>
										<div class="layui-equipment-number">
											<div>正常</div>
											<p>毫米波雷达</p>
											<i class="layui-equipment-normal"></i>
										</div>
										<div class="layui-equipment-number">
											<div>异常</div>
											<p>天线设备</p>
											<i class="layui-equipment-abnormal"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="layui-cen-botfour layui-four-border">
							<div class="layui-border">
								<div>
									<p><label>点位名称：</label><span>sdf</span></p>
									<p>方&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;向：<span>sdf</span></p>
									<p>纬&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;度：<span>sdf</span></p>
									<p>车道数量：<span>sdf</span></p>
									<p>经&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;度：<span>sdf</span></p>
								</div>
								<div>
									<p>设备编号：<span>sdf</span></p>
									<p>设备名称：<span>sdf</span></p>
									<p>设备品牌：<span>sdf</span></p>
									<p>设备型号：<span>sdf</span></p>
									<p>设&nbsp;&nbsp;&nbsp备&nbsp;IP：<span>sdf</span></p>
								</div>
							</div>
						</div>
					</div>
					<div class="layui-botton-rigth">
						<div class="layui-rigth-topfour layui-four-border">
							<div class="layui-border">
								<h3>历史维修数据<a href="#">更多&gt;</a></h3>
								<div class="layui-history-maintain">
									<p><label>异常时间：</label><span>2020-10-20 13:01:30</span></p>
									<p><label>异常记录：</label><span>称重台1车道石英损坏，维修</span></p>
									<p><label>处理人：</label><span>叶曰桃 2020-10-20 13:01:30</span></p>
								</div>
								<div class="layui-history-maintain">
									<p><label>异常时间：</label><span>2020-10-20 13:01:30</span></p>
									<p><label>异常记录：</label><span>称重台1车道石英损坏，维修</span></p>
									<p><label>处理人：</label><span>叶曰桃 2020-10-20 13:01:30</span></p>
								</div>
							</div>
						</div>
						<div class="layui-rigth-botfour layui-four-border">
							<div class="layui-border">
								<h3>巡检记录<a href="#">更多&gt;</a></h3>
								<div class="layui-polling-record">
									<p><label>巡检时间：</label><span>2020-10-20 13:01:30</span><i class="layui-already-polling">已巡检</i></p>
									<p><label>巡检内容：</label><span>清理机柜，检查设备</span></p>
									<p><label>巡检照片：</label><a class="layui-polling-examine" href="javascript:(0)">查看</a></p>
								</div>
								<div class="layui-polling-record">
									<p><label>巡检时间：</label><span>2020-10-20 13:01:30</span><i class="layui-stay-polling">待巡检</i></p>
									<p><label>巡检内容：</label><span>清理机柜，检查设备</span></p>
									<p><label>巡检照片：</label><a class="layui-polling-examine" href="javascript:(0)">查看</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
		<script src="../js/lay-config.js?v=1.0.4" charset="utf-8"></script>
		<script>
			layui.use(['tree', 'util'], function(){
			  var tree = layui.tree
			  ,$ = layui.jquery
			  ,layer = layui.layer
			  ,util = layui.util
			   //模拟数据1
			    ,data1 = [{
			      title: '江西'
			      ,id: 1
			      ,children: [{
			        title: '南昌'
			        ,id: 1000
			        ,children: [{
			          title: '青山湖区'
			          ,id: 10001
			        },{
			          title: '高新区'
			          ,id: 10002
			        }]
			      },{
			        title: '九江'
			        ,id: 1001
			      },{
			        title: '赣州'
			        ,id: 1002
			      }]
			    },{
			      title: '广西'
			      ,id: 2
			      ,children: [{
			        title: '南宁'
			        ,id: 2000
			      },{
			        title: '桂林'
			        ,id: 2001
			      }]
			    },{
			      title: '陕西'
			      ,id: 3
			      ,children: [{
			        title: '西安'
			        ,id: 3000
			      },{
			        title: '延安'
			        ,id: 3001
			      }]
			    }];
				tree.render({
				   elem: '#layui-botton-tree'
				   ,data: data1
				   ,showLine: false  //是否开启连接线
				 });
				 setInterval(function(){
					 var systemDate = new Date();
					 var year=systemDate.getFullYear();    // 获取完整的年份(4位,1970-????)
					 var month=systemDate.getMonth()+1;       // 获取当前月份(0-11,0代表1月)
					 var date=systemDate.getDate();        // 获取当前日(1-31)
					 var hours=systemDate.getHours();       // 获取当前小时数(0-23)
					 var minutes=systemDate.getMinutes();     // 获取当前分钟数(0-59)
					 var seconds=systemDate.getSeconds();     // 获取当前秒数(0-59)
					 
					 $(".layui-ele h2").html(year+"年"+formatZero(month,2)+"月"+formatZero(date,2));
					 $(".layui-ele p").html(formatZero(hours,2)+":"+formatZero(minutes,2)+":"+formatZero(seconds,2));
				 },1000);
				 function formatZero(num, len) {
				     if(String(num).length > len) return num;
				     return (Array(len).join(0) + num).slice(-len);
				 }
			  });
			  
			 
		</script>
	</body>
</html>
