<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<#--		<link rel="stylesheet" href="../lib/layui-v2.5.5/css/layui.css" media="all">-->
		<link rel="stylesheet" href="static/layui/css/layui.css">
<#--		<link rel="stylesheet" href="../lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">-->
<#--		<link rel="stylesheet" href="../css/public.css" media="all">-->
		<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css" media="all">
		<link rel="stylesheet" href="../font-awesome-4.7.0/css/public.css" media="all">




		<style>
			/* .layui-bgcol{background-color: #002e44}
			.layui-topbd{}
			.layui-float{float: left;}
			.layui-float-left{padding-left:1rem;}
			.layui-con{padding:0.3rem 0px;border-top: 0.125rem solid rgb(108,196,255);color: white;width: 250px;position: relative;}
			.layui-con::after{content: '';display: block;clear: both;width: 112px;height: 50px;position: absolute;left: 219px;top: 29px;transform: rotate(42deg);border-top: 2px solid rgb(108,196,255);}
			.layui-float-left h2{line-height: 3.125rem;} */
			body{min-width: 1306px;color: white;}
			.layuimini-container{background-color: rgb(9,32,61);height: 632px;}
			.layuimini-main{margin: 1.3rem 2px 0.3rem;}
			.layui-float-left>div{height: 5.625rem;}
			.layui-float{float: left;}
			.layui-left-bdcon{border-top: 0.125rem solid rgb(108,196,255);width: 16.25rem;padding: 0px 0px 12px 1rem;}
			.layui-ele h2{line-height: 50px;}
			.layui-left-bdcon::after{width: 6.875rem;height: 1px;position: relative;left: 14.9375rem;top: -1.6875rem;transform: rotate(50deg);border-top: 2px solid rgb(108,196,255);}
			.layui-right-bdcon{border-top: 0.125rem solid rgb(108,196,255);width: 16.25rem;padding: 0px 0px 12px 1rem;}
			.layui-right-bdcon::after{width: 6.875rem;height: 1px;position: relative;left: -6.625rem;top: -1.625rem;transform: rotate(310deg);border-top: 2px solid rgb(108,196,255);}
			.layui-float-cen{width: 37.9375rem;margin: 0px 4.375rem;}
			.layui-cen{height: 5.25rem;border-bottom: 0.125rem solid rgb(108,196,255);padding: 0px 4px;}
			.layui-cen>div{float: left;}
			.clear::after{content: '';display: block;clear: both;}
			.layui-cen-a>a{line-height: 42px;width: 120px;display: inline-block;text-align: center;color: white;font-size: 17px;border-radius: 5px;}
			.layui-cen-a{margin-top: 32px;}
			.layui-cen>div:nth-child(2){font-size: 16px;width: 354px;text-align: center;color: rgb(141,192,252);}
			.layui-cen>div:nth-child(1)>a{background-color: rgb(30,159,255);border: 1px solid rgb(30,159,255);}
			.layui-cen>div:nth-child(3)>a{background-color: rgb(35,84,140);border: 1px solid rgb(141,192,252);}
			.layui-cen h1{white-space: nowrap;overflow: hidden;text-overflow: ellipsis;color: rgb(141,192,252);}
			.layui-botton-con{margin: 0px 16px;}
			.layui-botton-con>div{float: left;}
			.layui-botton-left{width: 315px;}
			.layui-four-border{background: linear-gradient(to left, rgb(108,196,255), rgb(108,196,255)) 
								left top no-repeat,linear-gradient(to bottom, rgb(108,196,255), rgb(108,196,255)) left top no-repeat,
								linear-gradient(to left, rgb(108,196,255), rgb(108,196,255)) right top no-repeat,linear-gradient(to bottom, 
								rgb(108,196,255), rgb(108,196,255)) right top no-repeat,linear-gradient(to left, rgb(108,196,255), rgb(108,196,255))
								 left bottom no-repeat,linear-gradient(to bottom, rgb(108,196,255), rgb(108,196,255)) left bottom no-repeat,
								 linear-gradient(to left, rgb(108,196,255), rgb(108,196,255)) right bottom no-repeat,linear-gradient(to left, 
								 rgb(108,196,255), rgb(108,196,255)) right bottom no-repeat;background-size: 2px 14px, 14px 3px, 2px 14px, 14px 3px;padding: 2px 2px 2px 2px}
			.layui-border{border: 1px solid rgb(108 196 255 / 45%);background-color: rgb(17,55,100);}
			.layui-botton-leftcon{width: 313px;height: 490px;}
			.layui-botton-leftcon>h2{margin: 16px 0 0 16px;font-size: 18px;}
			.layui-botton-leftsrc{margin: 22px 0px 0px 8px;}
			.layui-botton-leftsrc input{height: 30px;width: 136px;margin-right: 8px;}
			.layui-botton-leftsrc>a{padding: 9px 15px;background-color: rgb(30,159,255);border-radius: 3px;color: white;}
			.layui-tree-txt{color: white;}
			.layui-tree-entry:hover{background-color: transparent;}
			.layui-cen-topfour.layui-rigth-topfour{height: 290px;}
			.layui-cen-botfour,.layui-rigth-botfour{margin-top: 10px;height: 186px;}
			.layui-botton-cen{width: 600px;margin: 0px 14px;}
			.layui-botton-rigth{ width: 320px;}
			.layui-cen-topfour>div{height: 289px;}
			.layui-rigth-botfour>div{height: 164px;padding: 10px;}
			.layui-cen-tone{border-bottom: 1px solid rgb(187,187,187);}
			.layui-cen-tone .layui-real-time{display: inline-block;background-color: rgb(30,159,255);width: 100px;text-align: center;line-height: 32px;border-radius: 3px;margin: 16px 0px 10px 10px;}
			.layui-cen-botfour>div>div{background-color: rgb(55,87,125);line-height: 30px;border: 1px solid rgb(187,187,187);width: 266px;display: inline-block;margin-left: 7px;margin-top: 7px;border-radius: 10px;padding: 3px 0px 3px 10px;}
			.layui-cen-botfour>div{padding: 7px;height: 170px;}
			.layui-cen-botfour p,.layui-history-maintain p{white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
			.layui-rigth-topfour>div{padding: 10px;height: 269px;}
			.layui-history-maintain{font-size: 12px;border: 1px solid rgb(187,187,187);border-radius: 10px;line-height: 30px;padding: 6px;margin-top: 22px;background-color: rgb(55,87,125);}
			.layui-rigth-topfour .layui-border>div:nth-last-child(1){margin-top: 15px;}
			.layui-polling-record{border-radius: 10px;background-color: rgb(108,196,240);font-size: 12px;padding: 5px;line-height: 18px;margin-top: 10px;}
			.layui-rigth-botfour .layui-border>div:nth-last-child(1){margin-top: 6px;}
			.layui-already-polling,.layui-stay-polling{color: #000000; position: relative;right: -56px;background-color:greenyellow;padding: 6px 10px;top: 6px;border-radius: 4px;font-size: 12px;}
			.layui-stay-polling{background-color: yellow}
			.layui-polling-examine{background-color: rgb(0,159,239);padding: 3px 8px;border-radius: 5px;color: white;}
			.layui-polling-examine:hover{color: white;}
			.layui-update-date{float: right;margin: 32px 12px 0px 0px;}
			.layui-equipment-number{position: relative;width: 160px;height: 76px;background-color: white;float: left;margin: 14px;border-radius: 5px;}
			.layui-cen-ctwo{padding: 10px 12px;overflow: auto;height: 208px;}
			.layui-equipment-number div{text-align: center;font-size: 16px;line-height: 79px;color: rgb(30,159,255);}
			.layui-equipment-number p{position: absolute;bottom: 0px;text-align: center;width: 100%;font-size: 13px;color: rgb(55,87,125);}
			.layui-equipment-number i{border-radius: 50%;width: 10px;height: 10px;position: absolute;top: 10px;right: 10px;}
			.layui-equipment-normal{background-color: greenyellow;}
			.layui-equipment-abnormal{background-color: red;}
		</style>

	</head>
	<body>
		<div class="layuimini-container layui-bgcol">
			<div class="layuimini-main">
				<div class="layui-row">
					<div class="layui-float layui-float-left">
						<div class="layui-left-bdcon clear">
							<div class="layui-ele">
								<h2>2020年7月10日</h2>
								<p>09:37:08</p>
							</div>
						</div>
					</div>
					<div class="layui-float layui-float-cen">
						<div class="layui-cen clear">
							<div class="layui-cen-a"><a href="javascript:void(0)">数据监控</a></div>
							<div><h1>通途西路K3+300</h1></div>
							<div class="layui-cen-a"><a href="javascript:void(0)">设备总览</a></div>
						</div>
					</div>
					<div class="layui-float layui-float-right">
						<div class="layui-right-bdcon clear">
							<div class="layui-ele">
								<h2>2020年7月10日</h2>
								<p>09:37:08</p>
							</div>
						</div>
					</div>
				</div>
				<div class="layui-row layui-botton-con">
					<div class="layui-botton-left layui-four-border">
						<div class="layui-botton-leftcon layui-border">
							<h2>点位信息</h2>
							<div class="layui-botton-leftsrc">
								<span>点位名称：</span>
								<input type="text" />
								<a href="javascript:(0)">搜索</a>
							</div>
							<div class="layui-botton-leftree">
								<div id="layui-botton-tree" class="demo-tree-more"></div>
							</div>
						</div>
					</div>
					<div class="layui-botton-cen">
						<div class="layui-cen-topfour layui-four-border">
							<div class="layui-border">
								<div class="layui-cen-tone clear">
									<p class="layui-real-time">实时数据</p>
									<p class="layui-update-date"><label>数据更新时间：</label><span>2020-10-20 15:10:30</span></p>
								</div>
								<div class="layui-cen-ctwo clear">
									<div class="layui-equipment-number">
										<div>运行中</div>
										<p>工控机</p>
										<i class="layui-equipment-normal"></i>
									</div>
									<div class="layui-equipment-number">
										<div>异常</div>
										<p>称台</p>
										<i class="layui-equipment-abnormal"></i>
									</div>
									<div class="layui-equipment-number">
										<div>正常</div>
										<p>抓拍机</p>
										<i class="layui-equipment-normal"></i>
									</div>
									<div class="layui-equipment-number">
										<div>正常</div>
										<p>LED显示屏</p>
										<i class="layui-equipment-normal"></i>
									</div>
									<div class="layui-equipment-number">
										<div>正常</div>
										<p>毫米波雷达</p>
										<i class="layui-equipment-normal"></i>
									</div>
									<div class="layui-equipment-number">
										<div>异常</div>
										<p>天线设备</p>
										<i class="layui-equipment-abnormal"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="layui-cen-botfour layui-four-border">
							<div class="layui-border">
								<div>
									<p><label>点位名称：</label><span>sdf</span></p>
									<p>方&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;向：<span>sdf</span></p>
									<p>纬&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;度：<span>sdf</span></p>
									<p>车道数量：<span>sdf</span></p>
									<p>经&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;度：<span>sdf</span></p>
								</div>
								<div>
									<p>设备编号：<span>sdf</span></p>
									<p>设备名称：<span>sdf</span></p>
									<p>设备品牌：<span>sdf</span></p>
									<p>设备型号：<span>sdf</span></p>
									<p>设&nbsp;&nbsp;&nbsp备&nbsp;IP：<span>sdf</span></p>
								</div>
							</div>
						</div>
					</div>
					<div class="layui-botton-rigth">
						<div class="layui-rigth-topfour layui-four-border">
							<div class="layui-border">
								<h3>历史维修数据</h3>
								<div class="layui-history-maintain">
									<p><label>异常时间：</label><span>2020-10-20 13:01:30</span></p>
									<p><label>异常记录：</label><span>称重台1车道石英损坏，维修</span></p>
									<p><label>处理人：</label><span>叶曰桃 2020-10-20 13:01:30</span></p>
								</div>
								<div class="layui-history-maintain">
									<p><label>异常时间：</label><span>2020-10-20 13:01:30</span></p>
									<p><label>异常记录：</label><span>称重台1车道石英损坏，维修</span></p>
									<p><label>处理人：</label><span>叶曰桃 2020-10-20 13:01:30</span></p>
								</div>
							</div>
						</div>
						<div class="layui-rigth-botfour layui-four-border">
							<div class="layui-border">
								<h3>巡检记录</h3>
								<div class="layui-polling-record">
									<p><label>巡检时间：</label><span>2020-10-20 13:01:30</span><i class="layui-already-polling">已巡检</i></p>
									<p><label>巡检内容：</label><span>清理机柜，检查设备</span></p>
									<p><label>巡检照片：</label><a class="layui-polling-examine" href="javascript:(0)">查看</a></p>
								</div>
								<div class="layui-polling-record">
									<p><label>巡检时间：</label><span>2020-10-20 13:01:30</span><i class="layui-stay-polling">待巡检</i></p>
									<p><label>巡检内容：</label><span>清理机柜，检查设备</span></p>
									<p><label>巡检照片：</label><a class="layui-polling-examine" href="javascript:(0)">查看</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<script src="../static/layui/layui.js" charset="utf-8"></script>




		<script src="../static/js/lay-config.js" charset="utf-8"></script>
		<script>
			layui.use(['tree', 'util'], function(){
			  var tree = layui.tree
			  ,$ = layui.jquery
			  ,layer = layui.layer
			  ,util = layui.util
			   //模拟数据1
			    ,data1 = [{
			      title: '江西'
			      ,id: 1
			      ,children: [{
			        title: '南昌'
			        ,id: 1000
			        ,children: [{
			          title: '青山湖区'
			          ,id: 10001
			        },{
			          title: '高新区'
			          ,id: 10002
			        }]
			      },{
			        title: '九江'
			        ,id: 1001
			      },{
			        title: '赣州'
			        ,id: 1002
			      }]
			    },{
			      title: '广西'
			      ,id: 2
			      ,children: [{
			        title: '南宁'
			        ,id: 2000
			      },{
			        title: '桂林'
			        ,id: 2001
			      }]
			    },{
			      title: '陕西'
			      ,id: 3
			      ,children: [{
			        title: '西安'
			        ,id: 3000
			      },{
			        title: '延安'
			        ,id: 3001
			      }]
			    }];
				tree.render({
				   elem: '#layui-botton-tree'
				   ,data: data1
				   ,showLine: false  //是否开启连接线
				 });
				 setInterval(function(){
					 var systemDate = new Date();
					 var year=systemDate.getFullYear();    // 获取完整的年份(4位,1970-????)
					 var month=systemDate.getMonth()+1;       // 获取当前月份(0-11,0代表1月)
					 var date=systemDate.getDate();        // 获取当前日(1-31)
					 var hours=systemDate.getHours();       // 获取当前小时数(0-23)
					 var minutes=systemDate.getMinutes();     // 获取当前分钟数(0-59)
					 var seconds=systemDate.getSeconds();     // 获取当前秒数(0-59)
					 
					 $(".layui-ele h2").html(year+"年"+formatZero(month,2)+"月"+formatZero(date,2));
					 $(".layui-ele p").html(formatZero(hours,2)+":"+formatZero(minutes,2)+":"+formatZero(seconds,2));
				 },1000);
				 function formatZero(num, len) {
				     if(String(num).length > len) return num;
				     return (Array(len).join(0) + num).slice(-len);
				 }
			  });
			  
			 
		</script>
	</body>
</html>
