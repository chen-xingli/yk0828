<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8">
		<title>首页二</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="../static/layui/css/layui.css" media="all">
		<link rel="stylesheet" href="../static/layui/css/font-awesome.min.css" media="all">
		<link rel="stylesheet" href="../static/layui/css/public.css" media="all">
		<link rel="stylesheet" href="../static/layui/css/main1119.css" media="all">
		<script src="static/js/jquery-3.4.1.min.js"></script>
		<script src="static/layui/layui.js"></script>
		<link rel="stylesheet" href="static/layui/css/layui.css"  media="all">
		<style>
			body{min-width: 1306px;}
			.layui-h3{padding-left:16px;}
			.layui-overview>.layui-col-md4{padding: 15px 56px;}
			.layui-overview>.layui-col-md4>div {padding: 10px;height:70px;color: #fff;}
			.layui-overview-pt {background-color:#0000ff96;}
			.layui-overview-equipment {background-color: #ffb800b3;}
			.layui-overview-overrun {background-color: #ff5722eb;}
			.layui-overview .layui-col-md4>img{width: 70px;height: 70px;}
			.layui-overview-matop8{margin-top: 8px;}
			.layui-map-pos{height:440px;border-radius: 13px;overflow: hidden;}
			.layui-map-con{background-color: white;height: 440px;margin-top: 10px;}
		</style>
	</head>
	<body>

	<div>

	</div>
		<div class="layuimini-container">
			<div class="layuimini-main">
				<div class="layui-row layui-col-space15">
					<!-- 系统概况 -->
					<div class="layui-col-md12">
						<h3 class="layui-h3">系统概况</h3>
						<div class="layui-col-md12 layui-overview layui-row">
							<div class="layui-col-md4">
								<div class="layui-overview-pt">
									<div class="layui-col-md4 layui-col-space2">
										<i class="layui-icon layui-icon-location" style="font-size: 65px; color: #620aff4d; height: 50px"></i>
									</div>
									<div class="layui-col-md8 layui-overview-matop8">
										<p>点位数量</p>
										<h2 class="layui-overview-matop8"><label> ${(stationcount)!}</label>台</h2>
									</div>
								</div>
							</div>
							<div class="layui-col-md4">
								<div class="layui-overview-equipment">
									<div class="layui-col-md4 layui-col-space2">
										<i class="layui-icon layui-icon-set-fill" style="font-size: 65px; color: #1e7eff4d; height: 50px"></i>
									</div>
									<div class="layui-col-md8 layui-overview-matop8">
										<p>设备数量</p>
										<h2 class="layui-overview-matop8"><label>${(devicecount)!}</label>台</h2>
									</div>
								</div>
							</div>
							<div class="layui-col-md4">
								<div class="layui-overview-overrun">
									<div class="layui-col-md4 layui-col-space2">
										<i class="layui-icon layui-icon-tips" style="font-size: 65px; color: #b9ff1e4d; height: 50px"></i>
									</div>
									<div class="layui-col-md8 layui-overview-matop8">
										<p>当月超限车数量</p>
										<h2 class="layui-overview-matop8"><label>1000</label>台</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="layuimini-container layui-map-con">
			<!-- 地图 -->
			<div class="layui-col-md12 layui-map-pos">
				<div id="container">

				</div>
			</div>
		</div>
		<script type="text/javascript" src="../static/layui/layui.js" charset="utf-8"></script>
		<script type="text/javascript" src="../static/js/lay-config.js?v=1.0.4"  charset="utf-8"></script>
		<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.15&key=3c915ea92589908217449ad48d06972c"></script>
		<script type="text/javascript">
			layui.use(['layer', 'miniTab', 'echarts'], function() {
				var $ = layui.jquery,
						layer = layui.layer,
						miniTab = layui.miniTab,
						echarts = layui.echarts;
				miniTab.listen();
			});


			//初始化地图对象，加载地图
			var map = new AMap.Map("container", {resizeEnable: true,
				expandZoomRange:true,
				zoom:19, //设置初始化级别
				zooms:[3,20] //设置缩放级别范围 3-20 级
			});

			var infoWindow = new AMap.InfoWindow({
				offset: new AMap.Pixel(0, -30),		//设置窗体的偏移量,默认水滴和窗体的箭头重合
				//				size: new AMap.Size(500, 0)			设置窗体的大小
			});

			lng();

			 function lng(){
				 $.ajax({
					 type:"GET",
					 url:'/getll',  //从数据库查询返回的是个list
					 dataType: "json",
					 async:false,
					 contentType: "application/x-www-form-urlencoded; charset=UTF-8",
					 success: function (data) {
					 	jwd(data);
					 }
				 })
			 }



			 function jwd(data){
				 $.each(data,function(index,item){
					 var longitude=item.lng
					 var latitude=item.lat
					 var stationName=item.stationName;
					 var direction=item.direction;
					 var marker = new AMap.Marker({
						 position: [longitude,latitude],
						 map: map
					 });
					 marker.content = '<h3>' + stationName + '</h3>';
					 marker.content += '<div>方向：' + direction + '</div>';
					 marker.content += '<div></div>';
					 marker.on('click', markerClick);
				 })
			 }
			    function markerClick(e) {
			        infoWindow.setContent(e.target.content);
			        infoWindow.open(map, e.target.getPosition());
			    }
			    map.setFitView();
		</script>
	</body>
</html>
