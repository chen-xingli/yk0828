<!-- 员工管理-->
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="/static/js/jquery-3.4.1.min.js"></script>
    <script src="/static/layui/layui.js"></script>
    <link rel="stylesheet" href="/static/layui/css/layui.css">
</head>
<style>


</style>
<body>

<!-- 卡片搜索面板-->
<div style="padding: 20px; background-color: #F2F2F2; width: 100%;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><span style="margin-right: 10px; margin-bottom: 2px" class="layui-badge-dot"></span>快速搜索</div>
                <div class="layui-card-body">
                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">点位名称</label>
                        <div class="layui-input-block" style="width: 200px">
                            <input id="ststionid" type="text" name="ststionid" required  lay-verify="required" placeholder="请输入点位名称" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">设备名称</label>
                        <div class="layui-input-block" style="width: 200px">
                            <input id="devicename" type="text" name="devicename" required  lay-verify="required" placeholder="请输入设备编号" autocomplete="off" class="layui-input">
                        </div>
                    </div>

<#--                    <div style="margin-bottom: 10px">-->
<#--                        <label class="layui-form-label">设备类型</label>-->
<#--                        <div class="layui-input-block" style="width: 200px">-->
<#--                            <input id="search-input-idcard" type="text" name="title" required  lay-verify="required" placeholder="请输入身份证" autocomplete="off" class="layui-input">-->
<#--                        </div>-->
<#--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use('laydate',function(){
        var laydate = layui.laydate;

        laydate.render({
            elem:'#submitime'
            ,format:'yyyy-MM-dd'
            ,done:function(value,date,startDate){
                //$scope.querySearchParams.startdatepicker = stitchingDate(value,'startdatepicker');
                $("#submitime").val(value).trigger('change');


            }
        });

        var enddatepicker=laydate.render({
            elem:'#submitime'
            ,format:'yyyy-MM-dd'
            ,done:function(value,date,startDate){
                //$scope.querySearchParams.startdatepicker = stitchingDate(value,'startdatepicker');

            }
        });
    });
</script>






<table class="layui-hide" id="employee-table" lay-filter="employee-table"></table>

<script type="text/html" id="toolbar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="addEmployee">添加设备信息</button>
    </div>
</script>

<script type="text/html" id="barTpl">
    <a class="layui-btn layui-btn-xs" lay-event="edit">保存</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>



<style type="text/css">
    .layui-table-cell{
        height:36px;
        line-height: 36px;
    }
</style>



<script>
    var tableContent = [];
    layui.use('table', function(){
        var table = layui.table;
        table.render({
            elem: '#employee-table',
            url:'/imformation',
            toolbar: '#toolbar',
            parseData: function (res) {
                console.log(res);
                tableContent = res.data;
                return {
                    "code": 0,
                    "msg": "",
                    "count": res.size,
                    data: res.data
                }
            }
            ,cols: [[
                { type: 'numbers', title: '序号', width: 30  },
                {field:'id', width:50, align:'center',title: '主键',hide:'true'},
                {field:'ststionid', width:80,align:'center', title: '点位ID',hide:'true'},
                {field:'stationidname', width:300,align:'center', title: '点位名称',templet:function(d){return '<div style = "text-align:left">'+d.stationidname+'</div>'}},
                {field:'devicename', width:150,align:'center', title: '设备名称',templet:function(d){return '<div style = "text-align:left">'+d.devicename+'</div>'}},
                {field:'devicebrand', width: 90,align:'center', title:'设备品牌'},
                {field:'devicemodel', width: 120,align:'center', title:'设备类型'},
                {field:'deviceversion', width: 210,align:'center', title:'设备对应版本',templet:function(d){return '<div style = "text-align:left">'+d.deviceversion+'</div>'}},
                {field:'ip', width: 120,align:'center', title:'IP地址', edit: true},
                {field:'online',templet:setState, width: 80,align:'center', title:'设备状态'},
                {field:'remark', width: 270,align:'left', title:'备注', edit: true,templet:function(d){return '<div style = "text-align:left">'+d.remark+'</div>'}},
                {fixed: 'right', title: '操作', align:'center', toolbar: '#barTpl'}
            ]]
            ,page: true
            ,limits: [3,5,8]
            ,limit: 8 //每页默认显示的数量
            // ,height: $(document).height() - $('#potentialcustomerDataGrid').offset().top - 20
            ,done: function (res, curr, count) {
                // 支持表格内嵌下拉框
                $(".layui-table-body, .layui-table-box, .layui-table-cell").css('overflow', 'visible')
                // 下拉框CSS重写 (覆盖父容器的CSS - padding)
                $(".laytable-cell-1-0-5").css("padding", "0px")
                $(".laytable-cell-1-0-7").css("padding", "0px")
                $(".laytable-cell-1-0-8").css("padding", "0px")
                $(".laytable-cell-1-0-5 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-7 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-8 span").css("padding", "0 10px")
                $("td").css("padding", "0px")
            }
        });


        function search(){
            var table = layui.table;
            table.render({
                elem: '#employee-table',
                url:'/imformation',
                toolbar: '#toolbar',
                parseData: function (res) {
                    console.log(res);
                    tableContent = res.data;
                    return {
                        "code": 0,
                        "msg": "",
                        "count": res.size,
                        data: res.data
                    }
                }
                ,cols: [[
                    { type: 'numbers', title: '序号', width: 30  },
                    {field:'id', width:50, align:'center',title: '主键',hide:'true'},
                    {field:'ststionid', width:80,align:'center', title: '点位ID',hide:'true'},
                    {field:'stationidname', width:280,align:'center', title: '点位名称'},
                    {field:'devicename', width:120,align:'center', title: '设备名称'},
                    {field:'devicebrand', width: 120,align:'center', title:'设备品牌'},
                    {field:'devicemodel', width: 120,align:'center', title:'设备类型'},
                    {field:'deviceversion', width: 120,align:'center', title:'设备对应版本'},
                    {field:'ip', width: 160,align:'center', title:'IP地址', edit: true},
                    {field:'online',templet:setState,  width: 80,align:'center', title:'设备状态'},
                    {field:'remark', width: 260,align:'left', title:'备注', edit: true},
                    {fixed: 'right', title: '操作', align:'center', toolbar: '#barTpl'}
                ]]
                ,page: true
                ,limits: [3,5,8]
                ,limit: 8 //每页默认显示的数量
                // ,height: $(document).height() - $('#potentialcustomerDataGrid').offset().top - 20
                ,done: function (res, curr, count) {

                    // 支持表格内嵌下拉框
                    $(".layui-table-body, .layui-table-box, .layui-table-cell").css('overflow', 'visible')
                    // 下拉框CSS重写 (覆盖父容器的CSS - padding)
                    $(".laytable-cell-1-0-5").css("padding", "0px")
                    $(".laytable-cell-1-0-7").css("padding", "0px")
                    $(".laytable-cell-1-0-8").css("padding", "0px")
                    $(".laytable-cell-1-0-5 span").css("padding", "0 10px")
                    $(".laytable-cell-1-0-7 span").css("padding", "0 10px")
                    $(".laytable-cell-1-0-8 span").css("padding", "0 10px")
                    $("td").css("padding", "0px")
                }
            });
        }

        function setState(data){
            var online=data.online;
            if(online=="在线"){
              return  "<span style='color: green'>在线</span>";
            }else {
                return  "<span style='color: red'>离线</span>";
            }
        }

        /* 搜索实现, 使用reload, 进行重新请求 */
        $("#ststionid").on('input',function () {
            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var ststionid = $("#ststionid").val();
            var devicename = $("#devicename").val();
            if (ststionid.length > 0) whereData["ststionid"] = ststionid;
            if (devicename.length > 0) whereData["devicename"] = devicename;
            table.reload("employee-table",{
                where: {
                    query: JSON.stringify(whereData)
                }
                ,page: {
                    curr: 1
                }
            });
        });


        $("#devicename").on('input',function () {
            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var ststionid = $("#ststionid").val();
            var devicename = $("#devicename").val();
            if (ststionid.length > 0) whereData["ststionid"] = ststionid;
            if (devicename.length > 0) whereData["devicename"] = devicename;
            table.reload("employee-table",{
                where: {
                    query: JSON.stringify(whereData)
                }
                ,page: {
                    curr: 1
                }
            });
        });



        table.on('toolbar(employee-table)', function (obj) {
            // 回调函数
            layerCallback= function(callbackData) {
                // 执行局部刷新, 获取之前的TABLE内容, 再进行填充
                var dataBak = [];
                var tableBak = table.cache.employee-table;
                for (var i = 0; i < tableBak.length; i++) {
                    dataBak.push(tableBak[i]);      //将之前的数组备份
                }
                // 添加到表格缓存
                dataBak.push(callbackData);
                //console.log(dataBak);
                table.reload("employee-table",{
                    data:dataBak   // 将新数据重新载入表格
                });
            };
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            switch(obj.event){
                case 'addEmployee':
                    layer.open({
                        title: '新建员工',
                        content: 'static/html/layers/device-insert.html',
                        type: 2,
                        offset: 't',
                        area: ["500px", "600px"],
                        success: function (layero, index) {
                            var iframe = window['layui-layer-iframe' + index];

                        }
                    });
            }
        });






        //监听工具条(右侧)
        table.on('tool(employee-table)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象

            if(layEvent === 'edit'){ //编辑

                // 发送更新请求
                $.ajax({
                    url: '/imformation',
                    method: 'put',
                    data: JSON.stringify({
                        id: data.id,
                        ststionid:data.ststionid,
                        devicename: data.devicename,
                        devicebrand: data.devicebrand,
                        devicemodel: data.devicemodel,
                        deviceversion: data.deviceversion,
                        ip: data.ip,
                        remark: data.remark
                    }),
                    contentType: "application/json",
                    success: function (res) {
                        console.log(res);
                        if (res.code == 200) {
                            layer.msg('更改员工信息成功', {icon: 1});
                            obj.update({
                                ip: data.ip,
                                remark: data.remark
                            });
                            search();
                        } else {
                            layer.msg('更改员工信息失败', {icon: 2});
                        }
                    }
                });
            } else if (layEvent == 'del') {
                layer.confirm('删除设备' + data.ststionid + '?', {skin: 'layui-layer-molv',offset:'c', icon:'0'},function(index){
                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                    layer.close(index);
                    //向服务端发送删除指令
                    $.ajax({
                        url: '/imformation/' + data.id,
                        type: 'delete',
                        success: function (res) {
                            console.log(res);
                            if (res.code == 200) {
                                layer.msg('删除成功', {icon: 1, skin: 'layui-layer-molv', offset:'c'});
                                search();
                            } else {
                                layer.msg('删除失败', {icon: 2, skin: 'layui-layer-molv', offset:'c'});
                            }
                        }
                    })
                });
            }
        });
    });
</script>
</body>
</html>