<!-- 员工管理-->
<!DOCTYPE html>
<html lang="en">
<head>

    <script src="/static/js/jquery-3.4.1.min.js"></script>
    <script src="/static/layui/layui.js"></script>
    <link rel="stylesheet" href="/static/layui/css/layui.css">
</head>
<style>


</style>
<body>

<!-- 卡片搜索面板-->
<div style="padding: 20px; background-color: #F2F2F2; width: 100%;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><span style="margin-right: 10px; margin-bottom: 2px" class="layui-badge-dot"></span>快速搜索</div>
                <div class="layui-card-body">
                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">巡检人</label>
                        <div class="layui-input-block" style="width: 200px">
                            <input id="inspecter" type="text" name="inspecter"  placeholder="请输入巡检人"  class="layui-input">
                        </div>
                    </div>

                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">巡检时间</label>
                        <div class="layui-input-block" style="width: 200px">
                            <select id="status" name="status" lay-verify="required"  lay-filter="business" class="layui-select">
                                <option value="" >全部</option>
                                <option value="1" >未完成</option>
                                <option value="0" >已完成</option>
                            </select>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>


<table class="layui-hide" id="employee-table" lay-filter="employee-table"></table>

<script type="text/html" id="toolbar">
    <div class="layui-btn-container">
    </div>
</script>

<script type="text/html" id="barTpl">
    {{# if(d.status=='未完成'){ }}
    <a class="layui-btn layui-btn-xs" lay-event="edit">巡检</a>
    {{# } }}
</script>
<script type="text/html" id="file">
    {{# if(d.status=='已完成'){ }}
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="filesearch">查看</a>
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="export" id="download-btn">导出</a>
    {{# } }}
    <#--    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="download" id="download-btn">下载</a>-->

</script>



<style type="text/css">
    .layui-table-cell{
        height:36px;
        line-height: 36px;
    }
</style>



<script>
    var tableContent = [];
    var layerCallback;
    layui.use('table', function(){
        var table = layui.table;
        table.render({
            elem: '#employee-table',
            url:'/inspect',
            toolbar: '#toolbar',
            parseData: function (res) {
                console.log(res);
                tableContent = res.data;
                return {
                    "code": 0,
                    "msg": "",
                    "count": res.size,
                    data: res.data
                }
            }
            ,cols: [[
                { type: 'numbers', title: '序号', width: 30  },
                {field:'id', width:50, align:'center',title: '主键',hide:'true'},
                {field:'stationid', width:80,align:'center', title: '点位ID',hide:'true'},
                {field: 'stationidname', width: 240, align: 'center', title: '点位名称',templet:function(d){return '<div style = "text-align:left">'+d.stationidname+'</div>'}},
                {field:'inspectdate', width:130,align:'center', title: '巡检时间',templet:'<div>{{ Format(d.inspectdate,"yyyy-MM-dd")}}</div>'},
                {field:'inspecter', width: 100,align:'center', title:'巡检人'},
                {field:'status', templet:setState,width:100,align:'center', title: '巡检状态'},
                {field:'inspectremark', width: 330,align:'left', title:'巡检情况说明',edit: true},
                {field:'right', width: 130,align:'center', title:'附件',toolbar: '#file'},
                {fixed: 'right', title: '操作', align:'center', toolbar: '#barTpl'}
            ]]
            ,page: true
            ,limits: [3,5,8]
            ,limit: 8 //每页默认显示的数量
            // ,height: $(document).height() - $('#potentialcustomerDataGrid').offset().top - 20
            ,done: function (res, curr, count) {
                // 支持表格内嵌下拉框
                $(".layui-table-body, .layui-table-box, .layui-table-cell").css('overflow', 'visible')
                // 下拉框CSS重写 (覆盖父容器的CSS - padding)
                $(".laytable-cell-1-0-5").css("padding", "0px")
                $(".laytable-cell-1-0-7").css("padding", "0px")
                $(".laytable-cell-1-0-8").css("padding", "0px")
                $(".laytable-cell-1-0-5 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-7 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-8 span").css("padding", "0 10px")
                $("td").css("padding", "0px")
            }
        });

        function setState(data){
            var status=data.status;
            if(status=="已完成"){
                return  "<span style='color: green'>已完成</span>";
            }else {
                return   "<span style='color: red'>未完成</span>";
            }
        }


        /* 搜索实现, 使用reload, 进行重新请求 */
        $("#inspecter").on('input',function () {
            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var inspecter = $("#inspecter").val();
            var status = $("#status").val();
            if (inspecter.length > 0) whereData["inspecter"] = inspecter;

            table.reload("employee-table",{
                where: {
                    query: JSON.stringify(whereData)
                }
                ,page: {
                    curr: 1
                }
            });
        });


        /* 搜索实现, 使用reload, 进行重新请求 */
        $("#status").on('input',function () {
            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var status = $("#status").val();

            if (status.length > 0) whereData["status"] = status;

            table.reload("employee-table",{
                where: {
                    query: JSON.stringify(whereData)
                }
                ,page: {
                    curr: 1
                }
            });
        });

        //监听工具条(右侧)
        table.on('tool(employee-table)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            // if(data.status=="未完成"){
                if(layEvent === 'edit'){ //编辑
                    layer.open({
                        title: '添加巡检',
                        type: 2,
                        content: 'static/html/layers/inspect-edit.html?id='+data.id+"&stationid="+data.stationid+"&INSPECTDATE="+data.INSPECTDATE,
                        offset: 't',
                        area: ["700px", "700px"],
                        end: function () {
                            location.reload();
                        }
                    })
                }
            // }else{

            // }
            if(layEvent === 'filesearch'){
                layer.open({
                    title: '附件查看',
                    type: 2,
                    content: 'static/html/layers/file-search.html?id='+data.id,
                    offset: 't',
                    area: ["700px", "500px"]
                })
            }    if(layEvent === 'export'){
                window.location.href = "/ce/" + data.id
            }

        });
    });
    //日期格式转换
    function Format(datetime,fmt) {
        if (parseInt(datetime)==datetime) {
            if (datetime.length==10) {
                datetime=parseInt(datetime)*1000;
            } else if(datetime.length==13) {
                datetime=parseInt(datetime);
            }
        }
        datetime=new Date(datetime);
        var o = {
            "M+" : datetime.getMonth()+1,                 //月份
            "d+" : datetime.getDate(),                    //日
            "h+" : datetime.getHours(),                   //小时
            "m+" : datetime.getMinutes(),                 //分
            "s+" : datetime.getSeconds(),                 //秒
            "q+" : Math.floor((datetime.getMonth()+3)/3), //季度
            "S"  : datetime.getMilliseconds()             //毫秒
        };
        if(/(y+)/.test(fmt))
            fmt=fmt.replace(RegExp.$1, (datetime.getFullYear()+"").substr(4 - RegExp.$1.length));
        for(var k in o)
            if(new RegExp("("+ k +")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        return fmt;
    }

</script>
</body>
</html>