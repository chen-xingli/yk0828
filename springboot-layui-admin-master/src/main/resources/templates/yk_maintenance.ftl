
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="../static/js/jquery-3.4.1.min.js"></script>
    <script src="../static/layui/layui.js"></script>
    <link rel="stylesheet" href="../static/layui/css/layui.css">


</head>
<style>


</style>
<body>

<!-- 卡片搜索面板-->
<div style="padding: 20px; background-color: #F2F2F2; width: 100%;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><span style="margin-right: 10px; margin-bottom: 2px"
                                                     class="layui-badge-dot"></span>快速搜索
                </div>
                <div class="layui-card-body ">
                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">点位名称</label>
                        <div class="layui-input-block" style="width: 200px">
                            <input id="stationid" type="text" name="stationid" required lay-verify="required"
                                   placeholder="请输入点位名称" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">上报时间</label>
                        <div class="layui-input-block" style="width: 200px">
                            <input id="submitime" type="text" name="submitime" required lay-verify="required"
                                   placeholder="请选择故障上报时间" autocomplete="off" class="layui-input">
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //常规用法
        laydate.render({
            elem: '#submitime'
        });

    });
</script>


<table class="layui-hide" id="employee-table" lay-filter="employee-table"></table>

<script type="text/html" id="file">
    <#--    {{# if(d.status=='已完成'){ }}-->
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="filesearch">查看</a>
    <#--    {{# } }}-->
</script>
<script type="text/html" id="file1">
    <#--    {{# if(d.status=='已完成'){ }}-->
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="filesearch1">查看</a>
    <#--    {{# } }}-->
</script>
<script>

</script>

<script type="text/html" id="toolbar">
    <div class="layui-btn-container">
        <#--        <button class="layui-btn layui-btn-sm" lay-event="addEmployee">添加记录</button>-->
    </div>
</script>

<script type="text/html" id="barTpl">
    <#--    <a class="layui-btn layui-btn-xs" lay-event="edit">保存</a>-->
    <a class="layui-btn layui-btn-xs" lay-event="deal">处理</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    {{# if(d.handleremark!='未处理'){ }}
    <a class="layui-btn layui-btn-xs" lay-event="output">导出记录</a>
    {{# } }}
</script>


<style type="text/css">
    .layui-table-cell {
        height: 36px;
        line-height: 36px;
        overflow:auto!important;

    }
    /*.mytable .layui-table-cell{*/
    /*    !*vertical-align: middle;*!*/

    /*    !*white-space:normal;*!*/
    /*}*/
    /*.layui-table-cell {*/
    /*    !*line-height: 17px !important; //可根据自己的样式来修改*!*/
    /*!*vertical-align: middle;*!*/
    /*    height: auto;*/
    /*    !*overflow: visible;*!*/
    /*    text-overflow: inherit;*/
    /*    !*white-space: normal;*!*/
    /*}*/
</style>


<script>


    var tableContent = [];
    layui.use('table', function () {
        var table = layui.table;
        table.render({
            elem: '#employee-table',
            url: '/selectMaintenance',
            method: 'get',
            toolbar: '#toolbar',
            parseData: function (res) {
                console.log(res);
                tableContent = res.data;
                return {
                    "code": 0,
                    "msg": "",
                    "count": res.size,
                    data: res.data
                }
            }
            , cols: [[
                {type: 'numbers', title: '序号', width: 30},
                {field: 'id', width: 50, align: 'center', title: '标识', hide: true},
                {field: 'stationid', width: 280, align: 'center', title: '点位名称',templet:function(d){return '<div style = "text-align:left">'+d.stationid+'</div>'}},
                {field: 'description', width: 250, align: 'center', title: '故障说明',templet:function(d){return '<div style = "text-align:left">'+d.description+'</div>'}},
                {field: 'handleremark', templet:setState,width: 100, align: 'center', title: '故障状态'},
                {field: 'deviceid', width: 100, align: 'center', title: '设备ID', hide: true},
                {
                    field: 'submittime',
                    width: 120,
                    align: 'center',
                    title: '故障上报时间',
                    templet: '<div>{{ Format(d.submittime,"yyyy-MM-dd")}}</div>'
                },
                {field: 'submitter', width: 100, align: 'center', title: '故障上报人'},
                {
                    field: 'handletime',
                    width: 120,
                    align: 'center',
                    title: '故障处理时间',
                    templet: panduan
                },

                {field: 'handler', width: 130, align: 'center', title: '故障处理人', edit: true},
                {field:'submitphoto', width: 130,align:'center', title:'故障附件',toolbar: '#file'},
                {field:'handlephoto', width: 130,align:'center', title:'故障处理附件',toolbar: '#file1'},
                {field: 'remark', width: 180, align: 'center', title: '处理说明'},
                {fixed: 'right', title: '操作', align: 'center', toolbar: '#barTpl'}


            ]]
            , page: true
            , limits: [3, 5, 8]
            , limit: 8 //每页默认显示的数量
            , done: function (res, curr, count) {
                // 支持表格内嵌下拉框
                $(".layui-table-body, .layui-table-box, .layui-table-cell").css('overflow', 'visible')
                // 下拉框CSS重写 (覆盖父容器的CSS - padding)
                $(".laytable-cell-1-0-5").css("padding", "0px")
                $(".laytable-cell-1-0-7").css("padding", "0px")
                $(".laytable-cell-1-0-8").css("padding", "0px")
                $(".laytable-cell-1-0-5 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-7 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-8 span").css("padding", "0 10px")
                $("td").css("padding", "0px")
                //换行
                $(".layui-table-main tr").each(function (index, val) {
                    $($(".layui-table-fixed-l .layui-table-body tbody tr")[index]).height($(val).height());
                    $($(".layui-table-fixed-r .layui-table-body tbody tr")[index]).height($(val).height());
                })
            }
        });
        function setState(data){
            var status=data.handleremark;
            if(status=="已处理"){
                return  "<span style='color: #49a149'>已处理</span>";
            }else {
                return   "<span style='color: #f10a0a'>未处理</span>";
            }
        }

        /* 搜索实现, 使用reload, 进行重新请求 */
        $("#stationid").on('input', function () {

            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var stationid = $("#stationid").val();
            var submitime = $("#submitime").val();
            if (stationid.length > 0) whereData["stationid"] = stationid;
            if (submitime.length > 0) whereData["submitime"] = submitime;
            table.reload("employee-table", {
                where: {
                    query: JSON.stringify(whereData)
                }
                , page: {
                    curr: 1
                }
            });
        });
        $("#submitime").on('input', function () {
            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var stationid = $("#stationid").val();
            var submitime = $("#submitime").val();
            if (stationid.length > 0) whereData["stationid"] = stationid;
            if (submitime.length > 0) whereData["submitime"] = submitime;
            table.reload("employee-table", {
                where: {
                    query: JSON.stringify(whereData)
                }
                , page: {
                    curr: 1
                }
            });
        });


        table.on('toolbar(employee-table)', function (obj) {
            // 回调函数
            layerCallback = function (callbackData) {
                // 执行局部刷新, 获取之前的TABLE内容, 再进行填充
                var dataBak = [];
                var tableBak = table.cache.employee - table;
                for (var i = 0; i < tableBak.length; i++) {
                    dataBak.push(tableBak[i]);      //将之前的数组备份
                }
                // 添加到表格缓存
                dataBak.push(callbackData);
                //console.log(dataBak);
                table.reload("employee-table", {
                    data: dataBak   // 将新数据重新载入表格
                });
            };
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            switch (obj.event) {
                case 'addEmployee':
                    layer.open({
                        title: '添加记录',
                        content: 'static/html/layers/maintenance-insert.html',
                        type: 2,
                        offset: 't',
                        area: ["1640px", "840px"],
                        success: function (layero, index) {
                            var iframe = window['layui-layer-iframe' + index];
                        }
                    });
            }
        });
        //监听工具条(右侧)
        table.on('tool(employee-table)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if (layEvent === 'edit') { //编辑
                console.log("remark更改:" + data.REMARK);
                // 发送更新请求
                $.ajax({

                    url: '/updateMaintenance',
                    method: 'put',
                    data: JSON.stringify({
                        ID: data.id,
                        REMARK: data.remark,
                        HANDLER: data.handler
                    }),
                    contentType: "application/json",
                    success: function (res) {
                        console.log(res);
                        if (res.code == 200) {
                            layer.msg('更改点位信息成功', {icon: 1});

                            obj.update({
                                remark: data.remark,
                                handler: data.handler
                            });
                            // search()
                        } else {
                            layer.msg('更改点位信息失败', {icon: 2});
                        }
                    }
                });

            } else if (layEvent == 'del') {

                layer.confirm('删除记录' + '：' + data.stationid + data.deviceid + data.description + '?', {
                    skin: 'layui-layer-molv',
                    offset: 'c',
                    icon: '0'
                }, function (index) {
                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                    layer.close(index);
                    //向服务端发送删除指令
                    $.ajax({
                        url: '/deleteMaintenance/' + data.id,
                        type: 'delete',
                        success: function (res) {
                            console.log(res);
                            if (res.code == 200) {
                                layer.msg('删除成功', {icon: 1, skin: 'layui-layer-molv', offset: 'c'});

                            } else {
                                layer.msg('删除失败', {icon: 2, skin: 'layui-layer-molv', offset: 'c'});
                            }
                        }
                    })
                });
            }
            else if (layEvent=='deal'){
                layer.open({
                    title:'故障处理',
                    type:2,
                    content:'static/html/layers/Dealmaintenance.html?id='+data.id,
                    offset:'t',
                    area:["700px","700px"],
                    end: function (){
                        location.reload();
                    }
                })
            }

            if(layEvent === 'filesearch'){
                layer.open({
                    title: '附件查看',
                    type: 2,
                    content: 'static/html/layers/file-search-yw.html?PTYPE=1&MAINTENANCEID='+data.id,
                    offset: 't',
                    area: ["700px", "500px"]
                })
            }else if (layEvent === 'filesearch1') {
                layer.open({
                    title: '附件查看',
                    type: 2,
                    content: 'static/html/layers/file-search-yw.html?PTYPE=2&MAINTENANCEID='+data.id,
                    offset: 't',
                    area: ["700px", "500px"]
                })
            }
            else if (layEvent=='output'){

                window.location.href="/files/outPut/" + data.id
            }
        });
    });


    function panduan(data){
        if(data.handletime==""||data.handletime==null){
            return "";
        }else{
            return  Format(data.handletime,"yyyy-MM-dd")
        }
    }

    function Format(datetime, fmt) {
        if (parseInt(datetime) == datetime) {
            if (datetime.length == 10) {
                datetime = parseInt(datetime) * 1000;
            } else if (datetime.length == 13) {
                datetime = parseInt(datetime);
            }
        }
        datetime = new Date(datetime);
        var o = {
            "M+": datetime.getMonth() + 1,                 //月份
            "d+": datetime.getDate(),                    //日
            "h+": datetime.getHours(),                   //小时
            "m+": datetime.getMinutes(),                 //分
            "s+": datetime.getSeconds(),                 //秒
            "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
            "S": datetime.getMilliseconds()             //毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }


    function search(){
        var table = layui.table;
        table.render({
            elem: '#employee-table',
            url: '/selectMaintenance',
            method: 'get',
            toolbar: '#toolbar',
            parseData: function (res) {
                console.log(res);
                tableContent = res.data;
                return {
                    "code": 0,
                    "msg": "",
                    "count": res.size,
                    data: res.data
                }
            }
            , cols: [[
                {type: 'numbers', title: '序号', width: 30},
                {field: 'id', width: 50, align: 'center', title: '标识', hide: true},
                {field: 'stationid', width: 200, align: 'center', title: '点位ID'},
                {field: 'deviceid', width: 80, align: 'center', title: '设备ID'},
                {field: 'description', width: 200, align: 'center', title: '故障说明'},
                {
                    field: 'submittime',
                    width: 120,
                    align: 'center',
                    title: '故障上报时间',
                    templet: '<div>{{ Format(d.submittime,"yyyy-MM-dd")}}</div>'
                },
                {field: 'submitter', width: 100, align: 'center', title: '故障上报人'},
                {field:'submitphoto', width: 130,align:'center', title:'故障附件',toolbar: '#file'},
                {field: 'handleremark', width: 100, align: 'center', title: '故障处理说明'},
                {
                    field: 'handletime',
                    width: 120,
                    align: 'center',
                    title: '故障处理时间',
                    templet: panduan
                },
                {field: 'handler', width: 100, align: 'center', title: '故障处理人', edit: true},
                {field:'handlephoto', width: 130,align:'center', title:'故障处理附件',toolbar: '#file1'},
                {field: 'remark', width: 100, align: 'left', title: '备注', edit: true},
                {fixed: 'right', title: '操作', align: 'center', toolbar: '#barTpl'}


            ]]
            , page: true
            , limits: [3, 5, 8]
            , limit: 8 //每页默认显示的数量
            , done: function (res, curr, count) {
                // 支持表格内嵌下拉框
                $(".layui-table-body, .layui-table-box, .layui-table-cell").css('overflow', 'visible')
                // 下拉框CSS重写 (覆盖父容器的CSS - padding)
                $(".laytable-cell-1-0-5").css("padding", "0px")
                $(".laytable-cell-1-0-7").css("padding", "0px")
                $(".laytable-cell-1-0-8").css("padding", "0px")
                $(".laytable-cell-1-0-5 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-7 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-8 span").css("padding", "0 10px")
                $("td").css("padding", "0px")
            }
        });
    }

</script>
</body>
</html>