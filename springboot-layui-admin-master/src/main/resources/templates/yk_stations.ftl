<!-- 员工管理-->
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="../static/js/jquery-3.4.1.min.js"></script>
    <script src="../static/layui/layui.js"></script>
    <link rel="stylesheet" href="../static/layui/css/layui.css">


</head>
<style>


</style>
<body>

<!-- 卡片搜索面板-->
<div style="padding: 20px; background-color: #F2F2F2; width: 100%;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><span style="margin-right: 10px; margin-bottom: 2px ;float: left" class="layui-badge-dot"></span>快速搜索</div>
                <div class="layui-card-body ">
                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">点位名称</label>
                        <div class="layui-input-block" style="width: 200px">
                            <input id="stationName" type="text" name="stationName" required  lay-verify="required" placeholder="请输入点位名称" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                    <div style="margin-bottom: 10px;float: left">
                        <label class="layui-form-label">设备类型</label>
                        <div class="layui-input-block" style="width: 200px">
                            <input id="deviceType" type="text" name="deviceType" required  lay-verify="required" placeholder="请输入设备类型" autocomplete="off" class="layui-input">
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<table class="layui-hide" id="employee-table" lay-filter="employee-table"></table>
<script type="text/html" id="toolbar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="addEmployee">添加点位信息</button>
        <button class="layui-btn layui-btn-sm" lay-event="addStationsByExcel">批量导入点位信息</button>
    </div>
</script>



<script type="text/html" id="barTpl">
    <a class="layui-btn layui-btn-xs" lay-event="edit">保存</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    <a class="layui-btn layui-btn-xs" lay-event="showById">查看详情</a>
    <a class="layui-btn layui-btn-xs" lay-event="addMaintenance">故障上报</a>
    <a class="layui-btn layui-btn-xs" lay-event="addPic">添加照片</a>
    <a class="layui-btn layui-btn-xs" lay-event="outPut">导出</a>
</script>
<script>

</script>

<style type="text/css">
    .layui-table-cell {
        height: 36px;
        line-height: 36px;
        overflow:auto!important;

    }
</style>



<script>
    var tableContent = [];
    layui.use('table', function() {
        var table = layui.table;
        table.render({
            elem: '#employee-table',
            url: '/station',
            method: 'get',
            toolbar: '#toolbar',
            parseData: function (res) {
                console.log(res);
                tableContent = res.data;
                return {
                    "code": 0,
                    "msg": "",
                    "count": res.size,
                    data: res.data
                }
            }
            , cols: [[
                {type: 'numbers', title: '序号', width: 30},
                {field: 'id', width: 50, align: 'center', title: '序号', hide: true},
                {field: 'stationName', width: 300, align: 'center', title: '点位名称',templet:function(d){return '<div style = "text-align:left">'+d.stationName+'</div>'}},
                {field: 'direction', width: 100, align: 'center', title: '路线方向'},
                // {field: 'stationShortName', width: 120, align: 'center', title: '点位朝向'},
                {field: 'areaName', width: 200, align: 'center', title: '行政区域名称'},
                // {field: 'roadCode', width: 100, align: 'center', title: '路线编号'},
                {field: 'roadName', width: 100, align: 'center', title: '路线名称'},
                {field: 'devicetype', width: 100, align: 'center', title: '设备类型'},

                {field: 'cycle', width: 100, align: 'center', title: '巡检周期(月)',edit:true},
                {field: 'firsttime', width: 200, align: 'center', title: '首次巡检日期', templet: panduan},
                {field:'remark', width: 150,align:'left', title:'备注',edit: true},
                {fixed: 'right', title: '操作', align: 'center', toolbar: '#barTpl'}


            ]]
            , page: true
            , limits: [3, 5, 8]
            , limit: 8 //每页默认显示的数量
            , done: function (res, curr, count) {
                // 支持表格内嵌下拉框
                $(".layui-table-body, .layui-table-box, .layui-table-cell").css('overflow', 'visible')
                // 下拉框CSS重写 (覆盖父容器的CSS - padding)
                $(".laytable-cell-1-0-5").css("padding", "0px")
                $(".laytable-cell-1-0-7").css("padding", "0px")
                $(".laytable-cell-1-0-8").css("padding", "0px")
                $(".laytable-cell-1-0-5 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-7 span").css("padding", "0 10px")
                $(".laytable-cell-1-0-8 span").css("padding", "0 10px")
                $("td").css("padding", "0px")
            }
        });

        /* 搜索实现, 使用reload, 进行重新请求 */
        $("#stationName").on('input', function () {
            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var stationName = $("#stationName").val();
            var deviceType = $("#deviceType").val();
            if (stationName.length > 0) whereData["stationName"] = stationName;
            if (deviceType.length > 0) whereData["deviceType"] = deviceType;
            table.reload("employee-table", {
                where: {
                    query: JSON.stringify(whereData)
                }
                , page: {
                    curr: 1
                }
            });
        });
        $("#deviceType").on('input', function () {
            // 用来传递到后台的查询参数MAP
            var whereData = {};
            var stationName = $("#stationName").val();
            var deviceType = $("#deviceType").val();
            if (stationName.length > 0) whereData["stationName"] = stationName;
            if (deviceType.length > 0) whereData["deviceType"] = deviceType;
            table.reload("employee-table", {
                where: {
                    query: JSON.stringify(whereData)
                }
                , page: {
                    curr: 1
                }
            });
        });


        table.on('toolbar(employee-table)', function (obj) {
            // 回调函数
            layerCallback = function (callbackData) {
                // 执行局部刷新, 获取之前的TABLE内容, 再进行填充
                var dataBak = [];
                var tableBak = table.cache.employee - table;
                for (var i = 0; i < tableBak.length; i++) {
                    dataBak.push(tableBak[i]);      //将之前的数组备份
                }
                // 添加到表格缓存
                dataBak.push(callbackData);
                //console.log(dataBak);
                table.reload("employee-table", {
                    data: dataBak   // 将新数据重新载入表格
                });
            };
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            switch (obj.event) {
                case 'addEmployee':
                    layer.open({
                        title: '新建点位',
                        content: 'static/html/layers/station-insert.html',
                        type: 2,
                        offset: 't',
                        area: ["1000px", "700px"],
                        success: function (layero, index) {
                            var iframe = window['layui-layer-iframe' + index];
                        }
                    });
                    return;
                case 'addStationsByExcel':
                    layer.open({
                        title: '从Excel中导入',
                        content:'static/html/layers/stationinput.html',
                        type: 2,
                        offset: 'auto',
                        area: ["500px", "150px"],
                        success: function (layero, index) {
                            var iframe = window['layui-layer-iframe' + index];

                        }
                    });
                    return;
            }
        });


        //监听工具条(右侧)
        table.on('tool(employee-table)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if (layEvent === 'edit') { //编辑
                console.log("remark更改:" + data.Remark);
                // 发送更新请求
                $.ajax({
                    url: '/updateStation',
                    method: 'put',
                    data: JSON.stringify({
                        ID: data.id,
                        // ststionid:data.ststionid,
                        // devicename: data.devicename,
                        // devicebrand: data.devicebrand,
                        // devicemodel: data.devicemodel,
                        // deviceversion: data.deviceversion,
                        Remark: data.remark,
                        Cycle:data.cycle,
                        FIRSTTIME:data.firsttime

                    }),
                    contentType: "application/json",
                    success: function (res) {
                        console.log(res);
                        if (res.code == 200) {
                            layer.msg('更改点位信息成功', {icon: 1});

                            obj.update({
                                remark: data.remark
                            });
                            search();
                        } else {
                            layer.msg('更改点位信息失败', {icon: 2});
                        }
                    }
                });

            } else if (layEvent == 'del') {

                layer.confirm('删除点位' + data.stationName + '?', {
                    skin: 'layui-layer-molv',
                    offset: 'c',
                    icon: '0'
                }, function (index) {
                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                    layer.close(index);
                    //向服务端发送删除指令
                    $.ajax({
                        url: '/deletestation/' + data.id,
                        type: 'delete',
                        success: function (res) {
                            console.log(res);
                            if (res.code == 200) {
                                layer.msg('删除成功', {icon: 1, skin: 'layui-layer-molv', offset: 'c'});
                                search();
                            } else {
                                layer.msg('删除失败', {icon: 2, skin: 'layui-layer-molv', offset: 'c'});
                            }
                        }
                    })
                });
            } else if (layEvent == 'showById') {
                layer.open({
                    title:'点位信息',
                    type:2,
                    content: 'static/html/layers/showStation.html?id='+data.id,
                    offset: 't',
                    area: ["1000px", "700px"],
                })
            }

            else if (layEvent=='addMaintenance'){
                layer.open({
                    title:'巡检报警',
                    type:2,
                    content:'static/html/layers/Addmaintenance.html?stationname='+data.stationName,
                    offset:'t',
                    area:["700px","700px"],
                })
            }
            else if (layEvent=='addPic'){
                layer.open({
                    title:'上传照片',
                    type:2,
                    content:'static/html/layers/stationaddpic.html?id='+data.id,
                    offset: 't',
                    area: ["500px", "500px"],
                })
            }
            else if (layEvent=='outPut'){
                window.location.href="/files/stoutPutByPoi/"+data.id
            }
        });

        function search() {
            var table = layui.table;
            table.render({
                elem: '#employee-table',
                url: '/station',
                method: 'get',
                toolbar: '#toolbar',
                parseData: function (res) {
                    console.log(res);
                    tableContent = res.data;
                    return {
                        "code": 0,
                        "msg": "",
                        "count": res.size,
                        data: res.data
                    }
                }
                , cols: [[
                    {type: 'numbers', title: '序号', width: 30},
                    {field: 'id', width: 50, align: 'center', title: '序号', hide: true},
                    {field: 'stationName', width: 220, align: 'left', title: '点位名称'},
                    {field: 'direction', width: 100, align: 'center', title: '路线方向'},
                    // {field: 'stationShortName', width: 120, align: 'center', title: '点位朝向'},
                    {field: 'areaName', width: 120, align: 'center', title: '行政区域名称'},
                    {field:'roadCode', width: 100,align:'center', title:'路线编号'},
                    {field:'roadName', width: 100,align:'center', title:'路线名称'},
                    {field: 'devicetype', width: 100, align: 'center', title: '设备类型'},
                    // {field:'stakeNumber', width: 100,align:'center', title:'站点桩号'},
                    // {field:'lng', width: 50, align:'center',title:'经度'},
                    // {field:'lat', width: 50,align:'center', title:'纬度'},
                    // {field:'deviceCode', width: 150 ,align:'left', title:'设备编号'},
                    // {field:'laneNumber', width: 50, align:'center',title:'车道'},

                    {field: 'cycle', width: 100, align: 'center', title: '巡检周期',edit: true},
                    {field: 'firsttime', width: 160, align: 'center', title: '首次巡检日期',edit:true, templet: panduan},

                    {field:'remark', width: 130,align:'left', title:'备注',edit: true},
                    {fixed: 'right', title: '操作', align: 'center', toolbar: '#barTpl'}
                ]]
                , page: true
                , limits: [3, 5, 8]
                , limit: 8 //每页默认显示的数量
                , done: function (res, curr, count) {
                    // 支持表格内嵌下拉框
                    $(".layui-table-body, .layui-table-box, .layui-table-cell").css('overflow', 'visible')
                    // 下拉框CSS重写 (覆盖父容器的CSS - padding)
                    $(".laytable-cell-1-0-5").css("padding", "0px")
                    $(".laytable-cell-1-0-7").css("padding", "0px")
                    $(".laytable-cell-1-0-8").css("padding", "0px")
                    $(".laytable-cell-1-0-5 span").css("padding", "0 10px")
                    $(".laytable-cell-1-0-7 span").css("padding", "0 10px")
                    $(".laytable-cell-1-0-8 span").css("padding", "0 10px")
                    $("td").css("padding", "0px")
                }
            });
        }
    })

    function Format(datetime, fmt) {
        if (parseInt(datetime) == datetime) {
            if (datetime.length == 10) {
                datetime = parseInt(datetime) * 1000;
            } else if (datetime.length == 13) {
                datetime = parseInt(datetime);
            }
        }
        datetime = new Date(datetime);
        var o = {
            "M+": datetime.getMonth() + 1,                 //月份
            "d+": datetime.getDate(),                    //日
            "h+": datetime.getHours(),                   //小时
            "m+": datetime.getMinutes(),                 //分
            "s+": datetime.getSeconds(),                 //秒
            "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
            "S": datetime.getMilliseconds()             //毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
    function panduan(data){
        if(data.firsttime==""||data.firsttime==null){
            return "";
        }else{
            return  Format(data.firsttime,"yyyy-MM-dd")
        }
    }


    layui.use('laydate', function() {
        var laydate = layui.laydate;
        //常规用法
        laydate.render({
            type: 'date',
            value: new Date(new Date())
        });

    });
    var time=new Date();
    var date = time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate() + ' ' + time.getHours()
        + ':' + time.getMinutes() + ':' + time.getSeconds();
    //执行实例
    laydate.render({
        elem: '#firsttime' //指定元素
        ,type: 'datetime'
    });


</script>
</body>
</html>